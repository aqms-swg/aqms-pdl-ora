## Changes made to code for Oracle  

The following two code files have been modified to make pdl-to-aqms compatible with Oracle.  

*  aqms-pdl/libs/libs_db.py  


diff aqms-pdl-ora/aqms_pdl/libs/libs_db.py aqms-pdl/aqms_pdl/libs/libs_db.py 
```
4c4
< import cx_Oracle
---
> import psycopg2
7,10c7
<     if config['DB_NAME'] is None or \
<             config['DB_USER'] is None or \
<             config['DB_PASSWORD'] is None or \
<             config['DB_HOST'] is None:
---
>     if config['conn_string'] is None:
12d8
<         
15,17c11,12
<         print (config['DB_USER'], config['DB_PASSWORD'], config['DB_NAME'])
<         conn = cx_Oracle.connect(config['DB_USER'], config['DB_PASSWORD'], config['DB_NAME'])
<     except cx_Oracle.OperationalError as ex:
---
>         conn = psycopg2.connect(config['conn_string'])
>     except psycopg2.OperationalError as ex:
55,57c50,54
<     if found:        
<         config["sqlalchemy.url"] = "oracle+cx_oracle://{}:{}@{}".format(
<                 config['DB_USER'], config['DB_PASSWORD'], config['DB_NAME'])
---
>     if found:
> 
>         config["sqlalchemy.url"] = "postgresql://{}:{}@{}:{}/{}".format(
>                 config['DB_USER'], config['DB_PASSWORD'], config['DB_HOST'],
>                 config['DB_PORT'], config['DB_NAME'])
``` 

* aqms-pdl/pdl_to_aqms.py  

diff aqms-pdl-ora/aqms_pdl/pdl_to_aqms.py aqms-pdl/aqms_pdl/pdl_to_aqms.py

```
415,417c415,418
<             retval = None
<             retval = cursor.callfunc("epref.setprefmag_magtype", int, [evid,magid])
<             logger.debug("Stored proc epref.setprefmag_magtype")
---
>             query = "select * from epref.setprefmag_magtype(%s, %s)" % (evid, magid)
>             logger.debug("Stored proc query=%s" % query)
>             cursor.execute(query)
>             retval = cursor.fetchone()[0]
521c522
<     epoch_base=list(session.execute("select truetime.nominal2true(%d) from dual" % int(epoch)))
---
>     epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
535c536
<     epoch_base=list(session.execute("select truetime.nominal2true(%d) from dual" % int(epoch)))
---
>     epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
796c797
<         query = "select geo_region.inside_border('%s', %f, %f) from dual" % \
---
>         query = "select geo_region.inside_border('%s', %f, %f)" % \
```

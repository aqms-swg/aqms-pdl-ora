import argparse
import io
import numpy as np
import os
import sys
from pathlib import Path

from obspy.core.event import read_events
from obspy.geodetics.base import gps2dist_azimuth
from .libs.schema_new import Eventprefmag, Eventprefor, Eventprefmec, Origin, Event

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

from . import installation_dir

from .libs.libs_log import configure_logger
from .libs.libs_util import read_config, archive_quakeml
from .libs.libs_util import get_hash, put_hash, init_sqlitedb

from .libs.libs_aqms import (obspy_event_to_aqms, obspy_origin_to_aqms, obspy_magnitude_to_aqms,
                            obspy_arrival_to_aqms, obspy_amplitude_to_aqms)
from .libs.libs_aqms import (obspy_amplitude_to_assocamm, obspy_amplitude_to_assocamo,
                            obspy_arrival_to_assocaro, obspy_focal_mechanism_to_aqms,
                            aqmsprefor, aqmsprefmag, aqmsprefmec, summarize)
from .libs.libs_amps import scan_xml_amps, unassoc_amplitude_to_aqms
from .libs.libs_db import test_connection, configure
from .libs.libs_obs import scan_quakeml

KM_PER_DEG = 111.19

fname = 'qml_to_aqms'

def getEvid(event):
    '''
    <event publicID="quakeml:OK.isti.com/Event/OK/70037753" ns0:datasource="ok" ns0:dataid="ok70037753" ns0:eventsource
="ok" ns0:eventid="70037753">

AttribDict({'datasource': AttribDict({'value': 'ok', 'namespace': 'http://anss.org/xmlns/catalog/0.1', 'type': 'attribute'}), 'dataid': AttribDict({'value': 'ok70036948', 'namespace': 'http://anss.org/xmlns/catalog/0.1', 'type': 'attribute'}), 'eventsource': AttribDict({'value': 'ok', 'namespace': 'http://anss.org/xmlns/catalog/0.1', 'type': 'attribute'}), 'eventid': AttribDict({'value': '70036948', 'namespace': 'http://anss.org/xmlns/catalog/0.1', 'type': 'attribute'})})
    event.resource_id.id
    '''
    evid = None
    if 'eventid' in event.extra:
        evid = int(event.extra.eventid.value)

    if evid is None:
        print("Error scanning evid from event!  Exitting!")
        exit(2)

    return evid

def main():

    config = read_config()
    configure_logger(config, logfile="%s.log" % fname, levelString=None)

    args = process_cmd_line()
    quakemlfile = args.qmlfile
    logger.info("%s: Insert qmlfile:%s" % (fname, quakemlfile))

    auth = 'OK'
    RSN_source = 'OK'
    # max 8 chars
    subsource = 'qml2aqms'

    print("Connect to db")
    use_db = test_connection(config)
    if use_db:
        logger.info("%s: We have a database connection!" % fname)
        createSession(config)
    else:
        logger.warning("%s: Running without database connection!" % fname)

    found_PDL_orig = None

# 1. Read in the PDL quakeml file:
    #event, origin, magnitude, focal_mechanism = read_quakemlfile(quakemlfile)

    print("Read quakeml")
    cat = read_quakemlfile(quakemlfile)
    print("Read quakeml DONE")
    # First wipe out these evids + affiliated ids
    evids = []
    for ievt, event in enumerate(cat.events):
        evid = getEvid(event)
        evids.append(evid)
        #cmd += "%s " % evid
    evids.sort()

    session = Session()

    mccorn = (73000000, 74000000)
    walta = (74000000, 75000000)
    garlen = (76000000, 77000000)
    region = None
    for esa in [mccorn, walta, garlen]:
        evid1 = esa[0]
        evid2 = esa[1]
        if evids[0] > evid1 and evids[0] < evid2 and \
           evids[-1] > evid1 and evids[-1] < evid2 :
            region = esa
            break
    if region is None:
        logger.warning("Unable to determine evid range from XML evid range:%d - %d" %
                       (evids[0], evids[-1]))
        #exit(2)

    #print("query for existing evids")

    #rows = session.query(Event).filter(Event.evid>region[0]).\
                                #filter(Event.evid<region[1]).all()
    import subprocess
    import time

    print("delete existing evids")
    if evids:
        logger.info("The following evids, read from existing XML will be deleted:")
        for evid in evids:
            logger.info("evid: %d" % evid)
    #if rows:
        cmd = './sql.delete.sh '
        #cmd += ' '.join([str(event.evid) for event in rows])
        cmd += ' '.join([str(evid) for evid in evids])
        cmd += ' > /dev/null 2>&1'
        os.system(cmd)
        time.sleep(10)       # Give time for sql.delete.sh to complete

    print("delete existing evids DONE")
    session.close()
    session = Session()

    logger.info("Done deleting evids:%s-%s --> Insert new ones" %
                (evids[0], evids[-1]))

    for ievt, event in enumerate(cat.events):

        min_dist = None

        for origin in event.origins:
            fix_leap_seconds(origin, event)


        logger.info("Process Event:%d: n_origins:%d n_picks:%d n_amps:%d n_mags:%d" %
                    (ievt, len(event.origins), len(event.picks),
                     len(event.amplitudes), len(event.magnitudes)))

        evid = getEvid(event)
        #evid = getSequence('event', 1)[0]
        logger.info("%s: Request new evid from sequence returned: [evid=%s]" % (fname, evid))

        aqms_event= obspy_event_to_aqms(event, evid=evid, auth=auth, subsource=subsource)
        session.add(aqms_event)

        for iorg, origin in enumerate(event.origins):
            logger.info("Origin:%d: %s [method:%s] [narr:%d]" %
                        (iorg, origin.time, origin.method_id, len(origin.arrivals)))
            orid = getSequence('origin', 1)[0]
            logger.info("%s: Request new orid from sequence returned: [orid=%s]" % (fname, orid))

            if origin.resource_id == event.preferred_origin().resource_id:
                logger.info("%s:  Make orid=%d preferred" % (fname, orid))
                aqms_event.prefor = orid
                session.add(aqmsprefor(evid, orid))

            if origin.arrivals:
                arrs = [x for x in origin.arrivals if x.distance is not None]
                sorted_arrivals = sorted(arrs, key=lambda x: x.distance, reverse=False)
                #sorted_arrivals = sorted(origin.arrivals, key=lambda x: x.distance, reverse=False)
                #MTH: obspy distance is in deg and all AQMS lengths are km:
                min_dist = sorted_arrivals[0].distance * KM_PER_DEG

            aqms_orig = obspy_origin_to_aqms(event, orid=orid, evid=evid,
                                             auth=auth, subsource=subsource,
                                             min_dist=min_dist, obspy_origin=origin)

            gtype = set_gtype(config, RSN_source, aqms_orig.lat, aqms_orig.lon)
            if gtype is not None:
                aqms_orig.gtype = gtype
            else:
                logger.info("set_gtype() returned None --> origin.gtype not set")

            session.add(aqms_orig)
            session.flush()

            if origin.arrivals:
                aqms_arrivals = []
                assocaro_adds = []
                arids = getSequence('arrival', len(origin.arrivals))

                for arrival in origin.arrivals:
                    arid = arids.pop(0)
                    # agency_id: 'pr'  ?? use this ??
                    auth_arr = auth
                    if getattr(arrival, 'creation_info', None) and \
                       getattr(arrival.creation_info, 'agency_id', None):
                        auth_arr = arrival.creation_info.agency_id.upper()
                    else:
                        logger.debug("Arrival doesn't have creation_info --> use origin auth=[%s] for auth_arr" % (auth))

                    logger.debug("Convert arrival: arid=[%s] auth=[%s]" % (arid, auth))
                    aqms_arrivals.append( obspy_arrival_to_aqms(arrival, arid=arid, auth=auth, subsource=subsource) )
                    # assocaro - Data associating arrivals with origins
                    assocaro_adds.append( obspy_arrival_to_assocaro(arrival, arid=arid, orid=orid, auth=auth, subsource=subsource) )

                logger.info("Insert %d arrivals arid:%d -to- %d" % (len(aqms_arrivals), aqms_arrivals[0].arid, aqms_arrivals[-1].arid))

                session.bulk_save_objects(aqms_arrivals)
                session.bulk_save_objects(assocaro_adds)
                session.flush()


        for imag, magnitude in enumerate(event.magnitudes):
            logger.info("Magnitude:%d: type:%s method:%s val:%.2f" %
                        (imag, magnitude.magnitude_type, magnitude.method_id, magnitude.mag))
            magid = getSequence('magnitude', 1)[0]
            logger.info("%s: Request magid returned:%s" % (fname, magid))
            if magnitude.resource_id == event.preferred_magnitude().resource_id:
                logger.info("%s: Make magid:%d preferred" % (fname, magid))
                aqms_event.prefmag = magid

            magtype = magnitude.magnitude_type.lower()[-1] # ML --> 'l'  Mww --> 'w'
            #aqms_mag  = obspy_magnitude_to_aqms(magnitude, orid=orid, evid=evid, magid=magid, auth=auth,
            aqms_mag  = obspy_magnitude_to_aqms(magnitude, orid=aqms_event.prefor, evid=evid,
                                                magid=magid, auth=auth, magtype=magtype,
                                                subsource=subsource, min_dist=min_dist)
            session.add(aqms_mag)
            session.flush()
            #session.add(aqmsprefmag(evid, magid, magtype=aqms_mag.magtype))


        if event.amplitudes:
            #The picks are missing!
            amps = []
            for amp in event.amplitudes:
                if amp.pick_id is None:
                    logger.info("amp is missing pick --> search for it")
                    found = False
                    w = amp.waveform_id
                    seed_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                    for pk in event.picks:
                        w = pk.waveform_id
                        pk_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                        if pk_id == seed_id:
                            amp.pick_id = pk.resource_id
                            found = True
                            break
                    if not found:
                        w = amp.waveform_id
                        if w.channel_code == "HHN":
                            w.channel_code = "HHE"
                        else:
                            w.channel_code = "HHN"
                        seed_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                        for pk in event.picks:
                            w = pk.waveform_id
                            pk_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                            if w.station_code == amp.waveform_id.station_code:
                                amp.pick_id = pk.resource_id
                                found = True

                    amp.type = 'WAS'
                    if not found:
                        w = amp.waveform_id
                        seed_id = "%s.%s.%s.%s" % (w.network_code, w.station_code, w.location_code, w.channel_code)
                        logger.error("No pick found for event: evid:%d id:%s OT:%s amp:%s id:%s" %
                                     (aqms_event.evid, event.resource_id.id,
                                      event.preferred_origin().time, seed_id, amp.resource_id.id))
                    else:
                        amps.append(amp)
            event.amplitudes = amps

            ampids = getSequence('amplitude', len(event.amplitudes))
            logger.info("Got %d ampids" % len(ampids))
            aqms_amplitudes = []
            assocamm_adds = []
            assocamo_adds = []
            # These should be checked!
            orid = aqms_event.prefor
            magid = aqms_event.prefmag
            logger.info("Insert amps with orid:%s and magid:%s" % (orid, magid))
            # MTH: ATODO: Need to test if these amplitudes are in fact coda measurements and fill coda table accordingly
            for amplitude in event.amplitudes:
                ampid = ampids.pop(0)
                auth_amp = auth
                #if getattr(amplitude.pick_id.get_referred_object(), 'creation_info', None) and \
                #getattr(amplitude.pick_id.get_referred_object().creation_info, 'agency_id', None):
                if getattr(amplitude, 'creation_info', None) and \
                   getattr(amplitude.creation_info, 'agency_id', None):
                    auth_amp = amplitude.creation_info.agency_id.upper()
                else:
                    logger.debug("Amplitude doesn't have creation_info --> use origin auth=[%s] for auth_amp" % (auth))

                logger.debug("Convert amplitude: ampid=[%s] auth=[%s]" % (ampid, auth))
                logger.info("Convert amplitude: ampid=[%s] auth=[%s]" % (ampid, auth))

                aqms_amplitudes.append(obspy_amplitude_to_aqms(amplitude, ampid=ampid, auth=auth, subsource=subsource))

                st_mag = None
                #for station_mag in event.station_magnitudes:
                for station_mag in event.station_magnitudes:
                    if amplitude.resource_id == station_mag.amplitude_id:
                        st_mag = station_mag
                        break
                # assocamm - associates amps with mag
                assocamm_adds.append( obspy_amplitude_to_assocamm(ampid=ampid, magid=magid,
                                                                  auth=auth, station_mag=st_mag,
                                                                  subsource=subsource))
                # assocamo - associates amps with origin
                # Note: This will not set fields: delta & seaz in assocamo
                #       since they are not present in obspy amplitudes
                #       The fix would be to relate ampid scnl to arrival scnl and
                #       pull delta/seaz from the corresponding arrival
                assocamo_adds.append( obspy_amplitude_to_assocamo(ampid=ampid, orid=orid,
                                                                  auth=auth, subsource=subsource) )
            session.bulk_save_objects(aqms_amplitudes)
            session.bulk_save_objects(assocamm_adds)
            session.bulk_save_objects(assocamo_adds)
            logger.info("Insert %d amps ampid:%d -to- %d" % (len(aqms_amplitudes), aqms_amplitudes[0].ampid, aqms_amplitudes[-1].ampid))



# 5. Commit the session
    try:
        logger.info("commit the session")
        print("commit the session")
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

        # MTH: probably should check that this isn't a new event ...


    '''
    # Use stored proc to set preferred magnitude
    if insert_magnitude:
        # MTH: Use stored proc to determine if this mag is preferred
        #      If so, the procedure will update eventprefmag, event.prefmagid and origin.prefmagid
        logger.info("Set the preferred magnitude to  magid=%d" % magid)
        try:
            engine = engine_from_config(config)
            connection = engine.raw_connection()
            cursor = connection.cursor()
            query = "select * from epref.setprefmag_magtype(%s, %s)" % (evid, magid)
            logger.debug("Stored proc query=%s" % query)
            cursor.execute(query)
            retval = cursor.fetchone()[0]
            if retval == 2:
                logger.info("Stored procedure setprefmag returned val=%d" % retval)
            elif retval == 1:
                logger.warning("Stored procedure setprefmag returned val=%d" % retval)
            else:
                logger.error("Stored procedure setprefmag returned val=%d" % retval)
            cursor.close()
            connection.commit()
        finally:
            connection.close()

    # Look for any local origins in db that associate with this event
    if associate_with_local_origins:
        PDL_orid = orid
        local_orig = associate_local_origin(PDL_orid, RSN_source, assoc_thresh)
        if local_orig:
            logger.info("Local db orid:%d OT:%f <%.2f, %.2f> h=%.1f km "
                        "is the single match to this PDL origin (orid:%d)" %
                        (local_orig.orid, local_orig.datetime, local_orig.lat, local_orig.lon,
                         local_orig.depth, PDL_orid))

    '''
    logger.info("==== End of pdl_to_aqms  ============================================")

    return


from sqlalchemy import Sequence
from sqlalchemy import create_engine, engine_from_config
from sqlalchemy.orm import sessionmaker

Session = None

def createSession(config):
    global Session

    if Session:
        logger.warning("Session is already created")
        return

    try:
        #engine = engine_from_config(config, echo=True)
        engine = engine_from_config(config)
        Session = sessionmaker(bind=engine)
    except :
        raise

    #return Session

def getSequence(name, count):
    sequences = {'event':'evseq', 'origin':'orseq', 'arrival':'arseq',
                 'amplitude':'ampseq', 'magnitude':'magseq',
                 'mechanism':'mecseq',
                 'unassocamp':'unassocseq'}

    session = Session()

    nextids = []

    if name in sequences:
        seq = Sequence(sequences[name])
        for i in range(count):
            nextids.append(session.execute(seq))
    else:
        logger.error("getSequence: Error - Unrecognized sequence name=[%s]" % name)

    session.close()
    return nextids

def query_for_RSN_event_in_aqms(evid):
    '''
        Look up event evid in the origin table
        If found, return the origin
    '''

    session = Session()
    found = session.query(Origin).filter(Origin.evid==evid).all()
    session.close()

    if len(found) >= 1:
        orig = found[-1]
        return orig
    else:
        return None

# ATODO: What if there is > 1 origin with this locevid ?
#        Should look at the associated event and return
#        the preferred origin if set (?)
def query_for_pdl_event_in_aqms(pdl_id):
    '''
        Query origin table for origin.locevid == pdl_id
        If found return origin
    '''

    # See if this PDL event already exists in db
    # Query the database for origin.locevid == dataid
    # If it does, then get the evid and use this to
    #  update the event.version, create a new origin from the PDL one, and make
    #  this the preferred origin, preferred mag, etc

    session = Session()
    found = session.query(Origin).filter(Origin.locevid==pdl_id).all()
    session.close()

    if len(found) >= 1:
        orig = found[-1]
        return orig
    else:
        return None

def fix_unassoc_amp_leap_seconds(amp_dict):
    epoch = amp_dict['wstart'].timestamp
    orig_utc = amp_dict['wstart']
    session = Session()
    epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
    session.close()
    fixed_epoch = epoch_base[0][0]
    leap_secs = fixed_epoch - int(epoch)

    logger.info("fix_unassoc_amp wstart=[%s] --> epoch=[%s] + leap_secs=[%d] = [%s]" % \
                (amp_dict['wstart'], epoch, leap_secs, (amp_dict['wstart'].timestamp + leap_secs)))

    amp_dict['wstart'] = amp_dict['wstart'].timestamp + leap_secs
    amp_dict['datetime'] = amp_dict['datetime'].timestamp + leap_secs

def fix_leap_seconds(origin, event):
    epoch = origin.time.timestamp
    session = Session()
    epoch_base=list(session.execute("select truetime.nominal2true(%d)" % int(epoch)))
    session.close()
    fixed_epoch = epoch_base[0][0]
    leap_secs = fixed_epoch - int(epoch)
    otime = origin.time
    origin.time += leap_secs
    logger.debug("fix_leap: origin.time=[%s] + leap_secs=[%d] = new origin.time=[%s]" % \
                (otime, leap_secs, origin.time))
    for pick in event.picks:
        pick.time += leap_secs

    return

def read_quakemlfile(quakemlfile):

    try:
        with open(quakemlfile, 'r') as file:
            xml = file.read()
        anssInternal = False
        if 'anssevent:internalEvent' in xml:
            anssInternal = True
            logger.info("This is an anssevent:internalEvent quakeml")
            xml = xml.replace('anssevent:internalEvent', 'event')
            #xml = file.read().replace('anssevent:internalEvent', 'event')
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
    except:
        logger.error("Problem reading quakemlfile=[%s]" % quakemlfile)
        raise

    return cat

def compare_origins(quakeml_origin=None, aqms_origin=None):
    '''
        Compare incoming quakeml_origin to db aqms_origin for same PDL event
    '''

    matches = False
    origin = quakeml_origin
    found_orig = aqms_origin
    logger.info("quakeml_origin.latitude=%f aqms_origin.lat=%f\n" % 
                (quakeml_origin.latitude, float(aqms_origin.lat)))
    # Determine if origin is the same as the db origin (=found_orig)
    #timestamp in seconds, depth in m
    x = [origin.time.timestamp, origin.latitude, origin.longitude, origin.depth]
    y = [float(found_orig.datetime), float(found_orig.lat), float(found_orig.lon), float(found_orig.depth) * 1e3]
    close = np.isclose(x, y, rtol=1e-09, atol=0.0)
    if np.all(close):
        logger.info("%s: found_orig:%s is perfect match, do nothing" % (fname, found_orig.orid))
        matches = True

    return matches

def associate_local_origin(PDL_orid, RSN_source, assoc_thresh):

    local_origin = None

    session = Session()
    rows = session.query(Origin).filter(Origin.orid==PDL_orid).all()
    if len(rows) > 1:
        logger.warning("Query for PDL orid=%d returned >1 origins!", PDL_orid)
    elif len(rows) == 0:
        logger.warning("Query for PDL orid=%d returned no origin", PDL_orid)
    PDL_orig = rows[-1]

    matching_origins = query_origins_for_matching_origin(PDL_orig, assoc_thresh)
    # At this point we have 0-many matching_origins with orid != this PDL_orig
    #   We can further filter out any origins with auth == the local RSN
    #   and if there is just 1, then we have our local association.
    local_origins = []
    if matching_origins:
        for orig in matching_origins:
            if orig.auth == RSN_source:
                local_origins.append(orig)
                logger.debug("Local db orid:%d OT:%f <%.2f, %.2f> h=%.1f km matches PDL origin" %
                            (orig.orid, orig.datetime, orig.lat, orig.lon, orig.depth))
        if len(local_origins) == 1:
            local_origin = local_origins[0]
        #elif nfound > 1:
        else:
            logger.warning("More than 1 local origin associates with this PDL origin!")
            for orig in local_origins:
                logger.info("local orig: orid:%d evid:%d datetime:%s <%.3f, %.3f> h=%.2f" % 
                            (orig.orid, orig.evid, orig.datetime, orig.lat, orig.lon, orig.depth))

    else:
            logger.info("Query for PDL orid=%d found no matching local origins in db" % PDL_orid)

    return local_origin



class thresh:
    def __init__(self, time, km, depth):
        self.time = time
        self.km = km
        self.depth = depth

def query_origins_for_matching_origin(target_origin, thresh):
    '''
        return list of origins in local db within thresh
            of target_origin
    '''
    session = Session()
    t1 = float(target_origin.datetime) - thresh.time
    t2 = float(target_origin.datetime) + thresh.time
    rows = session.query(Origin).filter(Origin.datetime>=t1).\
                                 filter(Origin.datetime<=t2).all()
    session.close()

    matching_origins = []
    if rows:
        for origin in rows:
            if origin.orid != target_origin.orid \
              and compare_aqms_origins(target_origin, origin, thresh):
                matching_origins.append(origin)

    return matching_origins

def compare_aqms_origins(orig1, orig2, thresh):
    '''
        return True if orig1 and orig2 are within thresh.{time, km, depth}
    '''
    match = False
    tol = 1e-6

    lat1 = float(orig1.lat)
    lon1 = float(orig1.lon)
    lat2 = float(orig2.lat)
    lon2 = float(orig2.lon)

    t1 = float(orig1.datetime)
    t2 = float(orig2.datetime)

    h1 = float(orig1.depth)
    h2 = float(orig2.depth)

    if (np.fabs(lat1 - lat2) < tol) \
       and (np.fabs(lon1 - lon2) < tol):
        dist = 0.
    else:
        dist, az, baz = gps2dist_azimuth(lat1, lon1, lat2, lon2)

    if (np.fabs(t1 - t2) < thresh.time) \
       and (np.fabs(h1 - h2) < thresh.depth) \
       and (dist/1e3 < thresh.km):
        match = True

    return match


def check_for_RSN_evid(args, RSN_source=None):
    '''
        See if args.eventids has an id that begins with the
          2-char RSN code and if so, pull the rest of the evid
          to see if it corresponds to evids in the local (RSN) db.
    '''
    if len(args.eventids) > 1:
        logger.info("%s: Attention pdl_type:%s code:%s preferred_eventid:%s has >1 eventids:%s" %
                    (fname, args.type, args.code, args.preferred_eventid, args.eventids))
        found_eventid = None
        for evid in args.eventids:
            if evid[0:2] == RSN_source:
                found_eventid = evid
                break
        if found_eventid:
            preferred = False
            if found_eventid == args.preferred_eventid:
                preferred = True
            RSN_evid = found_eventid[2:]
            logger.info("%s: RSN_source=[%s] RSN_evid=[%s] preferred:[%s]" %
                        (fname, RSN_source, RSN_evid, preferred))
            return RSN_evid

    return None

def handle_unassociated_amp(quakemlfile, use_db=0):

    xml_amps_dir = quakemlfile
    amps = scan_xml_amps(xml_amps_dir)
    logger.info("%s: pdl_type:%s scan for amps in xml_dir=[%s] returned:[%d] unassoc amps" % \
                (fname, pdl_type, xml_amps_dir, len(amps)))
    if use_db:
        session = Session()
        ampids = getSequence('unassocamp', len(amps))
        AQMS_amps = []
        for amp in amps:
            ampid = ampids.pop(0)
            fix_unassoc_amp_leap_seconds(amp)
            AQMS_amps.append(unassoc_amplitude_to_aqms(amp, ampid=ampid, auth=auth, subsource=subsource))
        session.bulk_save_objects(AQMS_amps)
        session.commit()
        session.close()
        logger.info("%s: Done inserting unassocamps --> Exit" % fname)

    return

def set_gtype(config, source, lat, lon):

    '''
    returns:
        l - if lat,lon within authoritative region for this source (RSN)
        r - if lat,lon not within authoritative region for this source (RSN)
        None - if it can't be determined (e.g., if source not set)
    '''

    gtype = None

    logger.info("set_gtype called with source=%s lat=%f lon=%f" % (source, lat, lon))

    try:
        engine = engine_from_config(config)
        connection = engine.raw_connection()
        cursor = connection.cursor()
        query = "select geo_region.inside_border('%s', %f, %f)" % \
                (source, lat, lon);
        cursor.execute(query)
        retval = cursor.fetchone()[0]
        logger.info("query:[%s] returned val=%d" % (query, retval))
        if retval == 1:
            gtype = 'l'
        elif retval == 0:
            gtype = 'r'
        else:
            logger.warning("gtype was not successfully set by SP!")
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    logger.info("set_gtype return gtype=%s" % gtype)
    return gtype


import argparse

def process_cmd_line():

    # MTH: fix "'--property-...'" silliness:
    #for i,arg in enumerate(sys.argv):
        #sys.argv[i] = arg.replace('"', "")

    parser = argparse.ArgumentParser()
    optional = parser._action_groups.pop()
    required = parser.add_argument_group("required arguments")

    parser._action_groups.append(optional) # 

    required.add_argument("--qmlfile", type=str, required=True, metavar='// Specify quakeml filename')

    args, unknown = parser.parse_known_args()

    return args


if __name__ == "__main__":
    main()

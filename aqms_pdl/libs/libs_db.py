import logging
logger = logging.getLogger()

import cx_Oracle
def test_connection(config):

    if config['DB_NAME'] is None or \
            config['DB_USER'] is None or \
            config['DB_PASSWORD'] is None or \
            config['DB_HOST'] is None:
        return False
        

    try:
        print (config['DB_USER'], config['DB_PASSWORD'], config['DB_NAME'])
        conn = cx_Oracle.connect(config['DB_USER'], config['DB_PASSWORD'], config['DB_NAME'])
    except cx_Oracle.OperationalError as ex:
        logger.error("test_connection: Caught error:[%s]" % repr(ex))
        return False

    return True


import yaml
def configure(filename=None):
    """
        Creates dictionary to pass to a sqlalchemy.engine_from_config() call
    """
    import os

    config = {}

    DB_PARAMS = ['DB_HOST', 'DB_NAME', 'DB_PORT', 'DB_USER', 'DB_PASSWORD']

    conn_string = None
    found = True

    if filename:
        with open(filename, 'r') as ymlfile:
            config = yaml.load(ymlfile, Loader=yaml.FullLoader)

        for param in DB_PARAMS:
            if param not in config:
                found = False
                break
    else:
        config['DB_HOST'] = os.getenv("DB_HOST", "localhost")
        config['DB_NAME'] = os.getenv("DB_NAME", "aqms_ir")
        config['DB_PORT'] = os.getenv("DB_PORT", "5432")
        config['DB_USER'] = os.getenv("DB_USER", "trinetdb")
        config['DB_PASSWORD'] = os.getenv("DB_PASSWORD")
        if not config['DB_PASSWORD']:
            config['DB_PASSWORD'] = raw_input("Password for user {} on {}: ".format(
                                               config['DB_USER'],config['DB_NAME']))
    if found:        
        config["sqlalchemy.url"] = "oracle+cx_oracle://{}:{}@{}".format(
                config['DB_USER'], config['DB_PASSWORD'], config['DB_NAME'])

        config["conn_string"] = "host=%s dbname=%s port=%s user=%s password=%s" % \
                        (config['DB_HOST'], config['DB_NAME'], config['DB_PORT'],
                         config['DB_USER'], config['DB_PASSWORD'])
    return config

def configure2(filename=None):
    """
        Creates dictionary to pass to a sqlalchemy.engine_from_config() call
    """
    import os
    #cwd = os.getcwd()
    cwd = os.path.dirname(os.path.abspath(__file__))

    configuration = {}
    if filename:
        filename = os.path.join(cwd, filename)
        #logger.info("Inside configure: filename=%s" % filename)
        # file the configuration dictionary using parameters from file
        try:
            with open(filename) as fp:
                for line in fp:
                    if line[0] != "#":
                        k,v = line.split("=")
                        configuration[k.strip()] = v.strip()
                        print("k=%s v=%s type=%s" % (k,v,type(v)))
        except:
            raise
        #logger.info("Inside configure: Done reading file")
        DB_HOST = configuration['DB_HOST']
        DB_NAME = configuration['DB_NAME']
        DB_PORT = configuration['DB_PORT']
        DB_USER = configuration['DB_USER']
        DB_PASSWORD = configuration['DB_PASSWORD']
    else:
        DB_HOST = os.getenv("DB_HOST", "localhost")
        DB_NAME = os.getenv("DB_NAME", "aqms_ir")
        DB_PORT = os.getenv("DB_PORT", "5432")
        DB_USER = os.getenv("DB_USER", "trinetdb")
        DB_PASSWORD = os.getenv("DB_PASSWORD")
        if not DB_PASSWORD:
            DB_PASSWORD = raw_input("Password for user {} on {}: ".format(DB_USER,DB_NAME))

    configuration["sqlalchemy.url"] = "postgresql://{}:{}@{}:{}/{}".format(DB_USER,DB_PASSWORD,DB_HOST,DB_PORT,DB_NAME)
    return configuration

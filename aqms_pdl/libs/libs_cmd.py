import argparse
import os
import sys

import logging
logger = logging.getLogger()

from .libs_log import string_to_logLevel
from .. import installation_dir

from obspy.core.utcdatetime import UTCDateTime

def process_cmd_line(fname, config):

    # MTH: fix "'--property-...'" silliness:
    for i,arg in enumerate(sys.argv):
        sys.argv[i] = arg.replace('"', "")

    parser = argparse.ArgumentParser()
    optional = parser._action_groups.pop()
    required = parser.add_argument_group("required arguments")

    parser._action_groups.append(optional) # 

    required.add_argument("--status", type=str, required=True)
    required.add_argument("--code", type=str, metavar='// e.g., --code=hv71386392', required=True)
    required.add_argument("--directory", type=str, metavar='// path to quakeml.xml', required=True)
    required.add_argument("--type", type=str, metavar='// e.g., --type=phase_data', required=True)
    required.add_argument("--source", type=str, metavar='// e.g., --source=pr', required=True)

    optional.add_argument("--optional_arg")

# could have --type=origin --status=DELETE --delete --action=PRODUCT_ADDED
    optional.add_argument("--action", type=str, metavar='e.g., EVENT_UPDATED, EVENT_UPDATED, PRODUCT_ADDED')

    optional.add_argument("--eventids", type=list_str, metavar='list of eventids')

    optional.add_argument("--preferred-eventid", type=str, metavar='//e.g., --preferred-eventid=hv71386392')
    optional.add_argument("--preferred-eventsource", type=str, metavar='//e.g., --preferred-eventsource=hv')
    optional.add_argument("--preferred-eventsourcecode", type=str, metavar='//e.g., --preferred-eventsourcecode=71386392')

    #optional.add_argument("--preferred-eventtime", type=str)
    optional.add_argument("--preferred-eventtime", type=UTCDateTime)
    # MTH: internal-moment-tensor sets this to 'null' !
    #optional.add_argument("--preferred-magnitude", type=float)
    optional.add_argument("--preferred-magnitude", type=str)

    optional.add_argument("--preferred-latitude", type=float)
    optional.add_argument("--preferred-longitude", type=float)
    optional.add_argument("--preferred-depth", type=float)

    optional.add_argument("--property-origin-source", type=str)
    optional.add_argument("--property-magnitude", type=float)
    optional.add_argument("--property-magnitude-type", type=str)
    optional.add_argument("--property-derived-magnitude", type=float)
    optional.add_argument("--property-derived-magnitude-type", type=str)
    optional.add_argument("--property-magnitude-num-stations-used", type=int)
    optional.add_argument("--property-magnitude-source", type=str)
    optional.add_argument("--property-standard-error", type=float)
    optional.add_argument("--property-horizontal-error", type=float)
    optional.add_argument("--property-error-ellipse-azimuth", type=float)
    optional.add_argument("--property-minimum-distance", type=float)
    optional.add_argument("--property-azimuthal-gap", type=float)
    optional.add_argument("--property-event-type", type=str)
    optional.add_argument("--property-version", type=int)
    optional.add_argument("--property-num-stations-used", type=int)
    optional.add_argument("--property-num-phases-used", type=int)
    optional.add_argument("--property-review-status", type=str)

    optional.add_argument("--ignore-latitude-above", type=float, metavar='// Ignore latitude > than this')
    optional.add_argument("--ignore-latitude-below", type=float, metavar='// Ignore latitude < than this')
    optional.add_argument("--ignore-longitude-above", type=float, metavar='// Ignore longitude > than this')
    optional.add_argument("--ignore-longitude-below", type=float, metavar='// Ignore longitude < than this')
    optional.add_argument("--ignore-depth-above", type=float, metavar='// Ignore depth > than this')
    optional.add_argument("--ignore-depth-below", type=float, metavar='// Ignore depth < than this')
    optional.add_argument("--ignore-magnitude-below", type=float, metavar='// Ignore magnitude < than this')
    optional.add_argument("--ignore-magnitude-above", type=float, metavar='Ignore magnitude > than this')
    optional.add_argument("--ignore-sources", type=list_str_sources, metavar='// Ignore these sources, eg. =HV,US')
    optional.add_argument("--only-listen-to-sources", type=list_str_sources, metavar='// Only listen for these sources')
    optional.add_argument("--ignore-arrivals", action='store_true')
    optional.add_argument("--ignore-amplitudes", action='store_true')
    optional.add_argument("--debug", type=str)
    optional.add_argument('--loglevel', type=str, metavar='// --loglevel=DEBUG (or INFO,WARN,ERROR,etc)')
    optional.add_argument('--configFile', type=str, metavar='// --configFile=config.yml')
    optional.add_argument("--archive-quakeml", action='store_true')
    optional.add_argument('--archive-quakeml-dir', type=str, metavar='dir to store quakeml files')

    optional.add_argument("--ignore-event-after", type=str, metavar='// Ignore if event age > {10mins, 3hrs, 5days, 2weeks, 3months, etc}')
    optional.add_argument("--subsource", type=str, metavar='// --subsource=SEISAN')
    optional.add_argument("--set_selectflag_zero", action='store_true')
    #optional.add_argument("--set_selectflag_zero", action='store_true', metavar='// If flag, set selectflag=0 on all inserted PDL events')

    args, unknown = parser.parse_known_args()

    #parser.print_help()
    #exit()

    if args.loglevel:
        logger.setLevel(string_to_logLevel(args.loglevel))

    logger.info("==== Process PDL msg %s" % (("=" * 70)))
    logger.info("     code:[%s]    source:[%s]    status:[%s]   type:[%s]" % \
                (args.code, args.source, args.status, args.type))
    if args.preferred_eventid:
        logger.info("     preferred_eventid:[%s]  eventids:%s" % (args.preferred_eventid, args.eventids))

    if args.status.upper() == 'DELETE':
        logger.info("     status=DELETE ==> Stop processing")
        exit(0)

    if args.preferred_eventtime:
        lat = None
        lon = None
        if args.preferred_latitude:
            lat = "%.2f" % float(args.preferred_latitude)
        if args.preferred_longitude:
            lon = "%.2f" % float(args.preferred_longitude)

        if not args.preferred_magnitude or args.preferred_magnitude == 'null':
            logger.warning("--preferred-magnitude is not set!")
            if args.property_magnitude:
                logger.warning("--property-magnitude *is* set - use it as preferred")
                args.preferred_magnitude = args.property_magnitude
                args.preferred_magnitude_type = args.property_magnitude_type
            elif args.property_derived_magnitude:
                logger.warning("--property-derived-magnitude *is* set - use it as preferred")
                args.preferred_magnitude = args.property_derived_magnitude
                args.preferred_magnitude_type = args.property_derived_magnitude_type
            else:
                logger.error("No --preferred/--property magnitude set --> Exit")
                exit(2)
        else:
            args.preferred_magnitude = float(args.preferred_magnitude)

        logger.info("     preferred_eventtime:[%s]  lat:%s lon:%s dep:%s mag:%s" %
                    (args.preferred_eventtime, lat, lon,
                     args.preferred_depth, args.preferred_magnitude))

    elif args.type != 'unassociated_amplitude' and args.status.upper() != 'DELETE':
        logger.error("--preferred-eventtime not set --> Exit!")
        exit(2)

    '''
    # MTH: AFIK unassociated-amplitude is the only type that doesn't have origin lat/lon/etc set ?
    if args.type in {'unassociated-amplitude'}:
        ignore = check_source(args)
        if ignore:
            logger.info("%s: [code=%s] [status=%s] [source=%s] [type=%s]: Didn't pass filter:%s" % \
                        (fname, args.code, args.status, args.source, args.type, ignore))
            exit(0)
        quakemlfile = args.directory
        return quakemlfile, args
    '''

    if config is None:
        print("%s: Must have a config.yml file to run" % fname)
        print("%s: Either: 1)from cmd line --configFile=path-to-file or 2) ./config.yml in cwd" % fname)
        print("%s:     or: 3)put config.yml in INSTALLATION_DIR=%s" % (fname, installation_dir()))
        logger.error("%s: Must have a config.yml file to run" % fname)
        logger.error("%s: Either: 1)from cmd line --configFile=path-to-file or 2) ./config.yml in cwd" % fname)
        logger.error("%s:     or: 3)put config.yml in INSTALLATION_DIR=%s" % (fname, installation_dir()))
        exit(2)

    if not args.archive_quakeml:
        if 'archive_quakeml' in config:
            args.archive_quakeml = config['archive_quakeml']
    if not args.archive_quakeml_dir:
        if 'archive_quakeml_dir' in config:
            args.archive_quakeml_dir = config['archive_quakeml_dir']
    if args.archive_quakeml and not args.archive_quakeml_dir:
        logger.error("If you set archive_quakeml you *must* also set archive_quakeml_dir")
        exit(2)

    if 'pdl-types' in config:
        allowable_types = config['pdl-types']
    else:
        logger.error("%s: Must set pdl-types in config.yml to configure allowable PDL msg types to process" % fname)
        exit(2)


    ignore = False

    if args.type not in allowable_types:
        #logger.info("%s: [code=%s] [status=%s] [source=%s] [type=%s]: Type not in allowable_types" %
                    #(fname, args.code, args.status, args.source, args.type))
        logger.info("Type not in allowable_types")
        exit(0)

    if args.preferred_eventtime:
        now = datetime.datetime.now(datetime.timezone.utc)
        now_epoch = now.timestamp()
        args.event_age = now_epoch - args.preferred_eventtime.timestamp

        logger.info("        current_UTC_time:[%s]  age_of_event:[%.1f seconds]" %
                     (UTCDateTime(now_epoch), args.event_age))
        if args.ignore_event_after:
            args.max_event_age = parse_time_string(args.ignore_event_after)

    ignore = check_filters(args)

    if ignore:
        #logger.info("%s: [code=%s] [status=%s] [source=%s] [type=%s]: Didn't pass filter:%s" % \
                    #(fname, args.code, args.status, args.source, args.type, ignore))
        logger.info("Didn't pass filter:%s" % (ignore))
        exit(0)

# could have --type=origin --status=DELETE --delete --action=PRODUCT_ADDED

    if args.debug and args.debug == 'summarize-only':
        logger.info("PDL: [code=%s] [type=%s] [status=%s] [action=%s]" % \
                    (args.code, args.type, args.status, args.action))

        logger.info("PDL: [source=%s] preferred-eventsource=[%s] [preferred_eventid=%s] eventids=%s" % \
                    (args.source, args.preferred_eventsource, args.preferred_eventid, args.eventids))

        if args.preferred_magnitude:
            logger.info("PDL: [ origin: %s (%.2f, %.2f) h=%.2f M:%.2f]" % \
                        (args.preferred_eventtime, args.preferred_latitude, args.preferred_longitude,
                        args.preferred_depth, args.preferred_magnitude))

        logger.info("PDL: [dir=%s]" % (args.directory))
        #exit(0)


    #if args.status == "DELETE":
        #exit(0)

    # MTH: Fix this:
    if args.type in {'phase-data', 'origin', 'focal-mechanism', 'moment-tensor',
                     'internal-origin', 'internal-moment-tensor'}:

        quakemlfile = os.path.join(args.directory, 'quakeml.xml')
        #return quakemlfile, args
    else:
        quakemlfile = args.directory

    return quakemlfile, args


def check_source(args):
    if args.ignore_sources and args.source.upper() in args.ignore_sources or \
       args.only_listen_to_sources and args.source.upper() not in args.only_listen_to_sources:
        return 'source=%s did not pass filter' % args.source.upper()

    return False

def check_filters(args):

    if args.ignore_sources and args.source.upper() in args.ignore_sources or \
       args.only_listen_to_sources and args.source.upper() not in args.only_listen_to_sources:
        return 'source=%s did not pass filter' % args.source.upper()
    if args.ignore_latitude_below and args.preferred_latitude < args.ignore_latitude_below:
        return 'latitude=%.2f < min latitude=%.2f' % (args.preferred_latitude, args.ignore_latitude_below)
    elif args.ignore_latitude_above and args.preferred_latitude > args.ignore_latitude_above:
        return 'latitude=%.2f > max latitude=%.2f' % (args.preferred_latitude, args.ignore_latitude_above)
    elif args.ignore_longitude_below and args.preferred_longitude < args.ignore_longitude_below:
        return 'longitude=%.2f < min longitude=%.2f' % (args.preferred_longitude, args.ignore_longitude_below)
    elif args.ignore_longitude_above and args.preferred_longitude > args.ignore_longitude_above:
        return 'longitude=%.2f > max longitude=%.2f' % (args.preferred_longitude, args.ignore_longitude_above)
    elif args.ignore_depth_below and args.preferred_depth < args.ignore_depth_below:
        return 'depth=%.2f < min depth=%.2f' % (args.preferred_depth, args.ignore_depth_below)
    elif args.ignore_depth_above and args.preferred_depth > args.ignore_depth_above:
        return 'depth=%.2f > max depth=%.2f' % (args.preferred_depth, args.ignore_depth_above)
    elif args.ignore_magnitude_below and args.preferred_magnitude < args.ignore_magnitude_below:
        return 'mag=%.2f < min mag=%.2f' % (args.preferred_magnitude, args.ignore_magnitude_below)
    elif args.ignore_magnitude_above and args.preferred_magnitude > args.ignore_magnitude_above:
        return 'mag=%.2f > max mag=%.2f' % (args.preferred_magnitude, args.ignore_magnitude_above)

    elif args.ignore_event_after and args.preferred_eventtime \
        and args.event_age > args.max_event_age:
        return 'age=%.2f > max age=%.2f' % (args.event_age, args.max_event_age)

    return False


def list_str(values):
    return values.split(',')

def list_str_sources(values):
    return values.upper().split(',')


import re
import datetime
def parse_time_string(time_string):
    '''
    parse a time string like: "50 days" or "2 MINS" 
        into seconds
    '''

    seconds = None

    time_units = {"SECONDS": 1, "SECS": 1, "S": 1,
                  "MINUTES": 60, "MINS": 60, "M": 60,
                  "HOURS": 3600, "HRS": 3600, "H": 3600,
                  "DAYS": 86400, "D": 86400,
                  "WEEKS": 604800, "W": 604800,
                  "MONTHS": 2628002.88, "M": 2628002.88,
                  }

    m = re.match(r"^(?P<number>\d+)(?P<unit>\w+)$", time_string)
    if m is None:
        m = re.match(r"^(?P<number>\d+) (?P<unit>\w+)$", time_string)

    if m is not None:
        d = m.groupdict()
        if d['unit'].upper() in time_units:
            seconds = float(d['number']) * time_units[d['unit'].upper()]
        else:
            logger.error("--ignore-event-after=%s' --> units=[%s] is not a valid time unit" %
                  (time_string, d['unit']))
            exit(2)
    else:
        logger.error('Unable to parse --ignore-event-after=%s into an integer + unit, ' \
                     'e.g., 5mins, "5 minutes", "10 days", 2months, etc.' %
                     time_string)
        exit(2)

    return seconds



if __name__ == "__main__":
    main()

"""
    Classes that describe AQMS tables

    CISN/ANSS Parametric Information Schema

    http://ncedc.org/db/Documents/NewSchemas/PI/v1.6.2/PI.1.6.2/index.htm
"""
#import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import text
from sqlalchemy import Column, DateTime, Integer, Numeric, String, ForeignKey
from sqlalchemy import Sequence
from sqlalchemy import CheckConstraint, UniqueConstraint

# create the base class of all ORM classes
Base = declarative_base()

class Epochtimebase(Base):
    __tablename__ = "epochtimebase"

    base = Column(String(1), primary_key=True, nullable=False, info='T or N')
    ondate = Column(DateTime, primary_key=True, nullable=False)
    offdate = Column(DateTime)

    CheckConstraint("base in ('T', 'N')", name='timebasechk')

class Amp(Base):
    __tablename__ = "amp"

    ampid = Column(Integer, Sequence('ampseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric)
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    amplitude = Column(Numeric, nullable=False, info='Amplitude in appropriate units for type ')
    amptype = Column(String(8), info='Amplitude type')
    units = Column(String(4), nullable=False, info='Units of amplitude')
    ampmeas = Column(String(1), info='Amplitude measure')
    eramp = Column(Numeric, info='Uncertainty in amplitude measurement')
    flagamp = Column(String(4), info='This attribute is a flag to indicate whether amplitude is over P packet, S packet, entire waveform, etc')
    per = Column(Numeric, info='Signal period. This attribute is the period of the signal described by the amplitude record ')
    snr = Column(Numeric, info='Signal-to-noise ratio. This is an estimate of the signal relative to that of the noise immediately preceding it')
    tau = Column(Numeric, info='Coda duration (F-P time)')
    quality = Column(Numeric, info='This attribute describes the completeness of the time window of data. A complete time window is 1.0, an incomplete time window is 0.0.')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    cflag = Column(String(2), info='This flag indicates whether the amplitude value is below noise, on scale or clipped')
    wstart = Column(Numeric, nullable=False, info='Start of time window of amplitude measurement')
    duration = Column(Numeric, info='Duration of amplitude reading')
    lddate = Column(DateTime, server_default=text('NOW()'))

    # table level CHECK constraint.  'name' is optional.
    CheckConstraint('ampid > 0', name='amp01')
    CheckConstraint('amplitude > 0', name='amp02')
    CheckConstraint("ampmeas in ('0','1')", name='amp03')
    CheckConstraint("amptype in ('C','WA','WAS','WASF','PGA','PGV','PGD','WAC',\
                                 'WAU','IV2','SP.3','SP1.0','SP3.0','ML100','ME100',\
                                 'EGY','M0')", name='amp04')
    CheckConstraint('eramp>=0.0', name='amp06')
    CheckConstraint("flagamp in ('P','S','R','PP','ALL','SUR')", name='amp07')
    CheckConstraint('per>0.0', name='amp08')
    CheckConstraint('tau>0.0', name='amp09')
    CheckConstraint("units in ('c','s','mm','cm','m','ms','mss','cms','cmss',\
                    'mms','mmss','mc','nm','e','cmcms','none','dycm')", name='amp10')
    CheckConstraint('quality>=0.0 and quality<=1.0', name='amp11')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='amp12')
    CheckConstraint("cflag in cflag in ('bn', 'os','cl','BN','OS','CL')", name='amp13')

class Unassocamp(Base):
    __tablename__ = "unassocamp"

    ampid = Column(Integer, Sequence('unassocseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric)
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    amplitude = Column(Numeric, nullable=False, info='Amplitude in appropriate units for type ')
    amptype = Column(String(8), info='Amplitude type')
    units = Column(String(4), nullable=False, info='Units of amplitude')
    ampmeas = Column(String(1), info='Amplitude measure')
    eramp = Column(Numeric, info='Uncertainty in amplitude measurement')
    flagamp = Column(String(4), info='This attribute is a flag to indicate whether amplitude is over P packet, S packet, entire waveform, etc')
    per = Column(Numeric, info='Signal period. This attribute is the period of the signal described by the amplitude record ')
    snr = Column(Numeric, info='Signal-to-noise ratio. This is an estimate of the signal relative to that of the noise immediately preceding it')
    tau = Column(Numeric, info='Coda duration (F-P time)')
    quality = Column(Numeric, info='This attribute describes the completeness of the time window of data. A complete time window is 1.0, an incomplete time window is 0.0.')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    cflag = Column(String(2), info='This flag indicates whether the amplitude value is below noise, on scale or clipped')
    wstart = Column(Numeric, nullable=False, info='Start of time window of amplitude measurement')
    duration = Column(Numeric, info='Duration of amplitude reading')
    lddate = Column(DateTime, server_default=text('NOW()'))

    # table level CHECK constraint.  'name' is optional.
    CheckConstraint('ampid > 0', name='amp01')
    CheckConstraint('amplitude > 0', name='amp02')
    CheckConstraint("ampmeas in ('0','1')", name='amp03')
    CheckConstraint("amptype in ('C','WA','WAS','WASF','PGA','PGV','PGD','WAC',\
                                 'WAU','IV2','SP.3','SP1.0','SP3.0','ML100','ME100',\
                                 'EGY','M0')", name='amp04')
    CheckConstraint('eramp>=0.0', name='amp06')
    CheckConstraint("flagamp in ('P','S','R','PP','ALL','SUR')", name='amp07')
    CheckConstraint('per>0.0', name='amp08')
    CheckConstraint('tau>0.0', name='amp09')
    CheckConstraint("units in ('c','s','mm','cm','m','ms','mss','cms','cmss',\
                    'mms','mmss','mc','nm','e','cmcms','none','dycm')", name='amp10')
    CheckConstraint('quality>=0.0 and quality<=1.0', name='amp11')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='amp12')
    CheckConstraint("cflag in cflag in ('bn', 'os','cl','BN','OS','CL')", name='amp13')

class Arrival(Base):
    __tablename__ = "arrival"

    arid = Column(Integer, Sequence('arseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric, nullable=False)
    sta = Column(String(6), nullable=False)
    net = Column(String(6), nullable=True)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8))
    channelsrc = Column(String(8))
    seedchan = Column(String(3))
    location = Column(String(2))
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    qual = Column(String(1), info='Onset quality. This single-character flag is used to denote the sharpness of the onset of a seismic phase.')
    clockqual = Column(String(1), info='This field describes whether the clock quality associated with a particular arrival is known, and if so, is good or bad')
    clockcorr = Column(Numeric, info='This field indicates the amount of time to add to the arrival time if clock correction is known')
    ccset = Column(String(1), info='This field describes whether the clock correction has been added or applied to the arrivals in computing a hypocenter solution')
    fm = Column(String(2), info='First motion. This is a two-character indication of first motion.')
    ema = Column(Numeric, info='Emergence angle. This attribute is the emergence angle of an arrival, as observed at a three-component station or array. The value increases from the vertical direction towards the horizontal')
    azimuth = Column(Numeric, info='Station to event azimuth measured clockwise from true north. Identified from waveform analysis')
    slow = Column(Numeric, info='Observed slowness. This is the observed slowness of a wave as it sweeps across an array')
    deltim = Column(Numeric, info='Arrival time uncertainty.')
    delinc = Column(Numeric, info='Uncertainty in the angle. This attribute gives the standard deviation of the angle of emergence')
    delaz = Column(Numeric, info='Uncertainty in the azimuth.')
    delslo = Column(Numeric, info='Uncertainty in slowness.')
    quality = Column(Numeric, info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad.')
    snr = Column(Numeric, info='Signal-to-noise ratio.')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'), info='Load date. Date and time that the record was created, in Oracle date datatype')

    CheckConstraint('arid > 0', name='arrival01')
    CheckConstraint('azmiuth >= 0.0 and azimuth <= 360.0', name='arrival02')
    CheckConstraint('delaz > 0.0', name='arrival03')
    CheckConstraint('delinc >= 0.0', name='arrival04')
    CheckConstraint('delslo > 0.0', name='arrival05')
    CheckConstraint('deltim >= 0.0', name='arrival06')
    CheckConstraint('ema >= 0.0 and ema <= 90.0', name='arrival07')
    CheckConstraint("fm in ('cu','cr','c.','du','dr','d.','.u','.r','..','+u','+r',\
                            '+.','-u','-r','-.')", name='arrival08')
    CheckConstraint("qual in ('i','e','w','I','E','W')", name='arrival09')
    CheckConstraint('slow >= 0.0', name='arrival10')
    CheckConstraint('snr > 0.0', name='arrival11')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='arrival12')
    CheckConstraint('ccset < 1', name='arrival13')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='arrival14')

class Coda(Base):
    __tablename__ = "coda"

    coid = Column(Integer, Sequence('coseq'), primary_key=True, nullable=False)
    commid = Column(Integer)
    datetime = Column(Numeric)
    sta = Column(String(6), nullable=False)
    net = Column(String(6))
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), info='SEED channel name. The first character denotes the band code, the second for the instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the same network operator ')
    iphase = Column(String(8), info='Reported phase. This eight-character field holds the name initially given to a seismic phase.')
    codatype = Column(String(3), info='Coda type')
    afix = Column(Numeric, info='Nominal coda amplitude')
    afree = Column(Numeric, info='Free amplitude')
    qfix = Column(Numeric, info='Fixed coda decay constant')
    qfree = Column(Numeric, info='Free decay')
    tau = Column(Numeric, info='Coda duration (F-P time)')
    nsample = Column(Integer, info='Number of coda sample windows')
    rms = Column(Numeric, info='Square root of the sum of the squares of the coda residuals, divided by the number of observations')
    durtype = Column(String(3), info='Duration type')
    eramp = Column(Numeric, info='Uncertainty in amplitude measurement')
    units = Column(String(4), nullable=False, info='Units of amplitude')
    time1 = Column(Numeric, info='Time for pairs')
    amp1 = Column(Numeric, info='Amplitude for pairs')
    time2 = Column(Numeric, info='Time for pairs')
    amp2 = Column(Numeric, info='Amplitude for pairs')
    time3 = Column(Numeric, info='Time for pairs')
    amp3 = Column(Numeric, info='Amplitude for pairs')
    time4 = Column(Numeric, info='Time for pairs')
    amp4 = Column(Numeric, info='Amplitude for pairs')
    time5 = Column(Numeric, info='Time for pairs')
    amp5 = Column(Numeric, info='Amplitude for pairs')
    time6 = Column(Numeric, info='Time for pairs')
    amp6 = Column(Numeric, info='Amplitude for pairs')
    quality = Column(Numeric, info='This attribute describes the completeness of the time window of data. A complete time window is 1.0, an incomplete time window is 0.0.')
    algorithm = Column(String(15), info='Algorithm used. This is a brief textual description of the algorithm used to derive coda data')
    winsize = Column(Numeric, info='Window over which waveform absolute amplitudes averaged')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('afix >= 0.0', name='coda01')
    CheckConstraint('amp1 > 0.0', name='coda03')
    CheckConstraint('amp2 > 0.0', name='coda04')
    CheckConstraint('amp3 > 0.0', name='coda05')
    CheckConstraint('amp4 > 0.0', name='coda06')
    CheckConstraint('amp5 > 0.0', name='coda07')
    CheckConstraint('amp6 > 0.0', name='coda08')
    CheckConstraint("codatype in ('P','S')", name='coda09')
    CheckConstraint('coid > 0', name='coda10')
    CheckConstraint('nsample >= 0', name='coda11')
    CheckConstraint('rms >= 0', name='coda12')
    CheckConstraint('time1 > 0', name='coda13')
    CheckConstraint('time2 > 0', name='coda14')
    CheckConstraint('time3 > 0', name='coda15')
    CheckConstraint('time4 > 0', name='coda16')
    CheckConstraint('time5 > 0', name='coda17')
    CheckConstraint('time6 > 0', name='coda18')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='coda19')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='coda20')
    CheckConstraint("durtype in ('a','d','h')", name='coda21')

class Event(Base):
    __tablename__ = "event"

    evid = Column(Integer, Sequence('evseq'), primary_key=True, nullable=False,
                  info='Event identifier. Each event is assigned a unique integer which identifies it in a database. It is possible for several records in the Origin relation to have the same evid.')
    prefor = Column(ForeignKey('origin.orid'))
    prefmag = Column(ForeignKey('netmag.magid'))
    prefmec = Column(ForeignKey('mec.mecid'))
    commid = Column(Integer, info='Comment identification. This is a key used to point to free-form comments entered in the Remark relation.')
    auth = Column(String(15), nullable=False)
    subsource = Column(String(28), info='A second identifier to specify the system or process that derived the data.')
    etype = Column(ForeignKey('eventtype.etype'), nullable=False, info='This attribute is used to identify the type of seismic event, when known')
    selectflag = Column(Integer, info='This flag describes the definitive definition of an event. Helps to differentiate which event to use from two different real-time systems')
    lddate = Column(DateTime, server_default=text('NOW()'))
    version = Column(Integer, nullable=False, info='Event version number. This attribute is incremented each time the preferred origin, magnitude or mechanism of the event is updated')

    CheckConstraint('evid > 0', name='event02')


class Assocevents(Base):
    __tablename__ = "assocevents"

    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False, info='This should refer to event generated by local RSN source')
    evidassoc = Column(ForeignKey('event.evid'), primary_key=True, nullable=False, info='ID of other event, eg from PDL')
    commid = Column(Integer, info='Comment identification. This is a key used to point to free-form comments entered in the Remark relation.')
    lddate = Column(DateTime, server_default=text('NOW()'))
    __table_args__ = (UniqueConstraint('evid', 'evidassoc', name='_evid_evidassoc_uc'),)

class Eventprefmag(Base):
    __tablename__ = "eventprefmag"

    magtype = Column(String(6), primary_key=True, nullable=False, info='This character string is used to specify the type of the magnitude measure')
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    magid = Column(ForeignKey('netmag.magid'), nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("magtype in ('p','a','b','e','l','l1','l2','lg','c','s',\
                                 'w','z','B', 'un','d','h','n','dl')", name='EventPrefMag_CHKTYP')

class Eventprefmec(Base):
    __tablename__ = "eventprefmec"

    mechtype = Column(String(2), primary_key=True, nullable=False, info='Mechanism type')
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    mecid = Column(ForeignKey('mec.mecid'))
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('evid > 0', name='EvPMec01')
    CheckConstraint('mecid > 0', name='EvPMec02')
    CheckConstraint("mechtype IN ('FP','MT')", name='EvPMec03')

class Eventprefor(Base):
    __tablename__ = "eventprefor"

    type = Column(String(2), primary_key=True, nullable=False, info='Type of location')
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    orid = Column(ForeignKey('origin.orid'), nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))
    CheckConstraint("type in ('h','H','c','C','a','A','d','D','u','U')", name='EventPrefOr_CHKTYP')

class Eventtype(Base):
    __tablename__ = "eventtype"

    etype = Column(String(2), primary_key=True, nullable=False,
                   info ='This attribute is used to identify the type of seismic event')
    name = Column(String(16), nullable=False, info='Name associated with an event type')
    description = Column(String(80), info='Event type description')

    CheckConstraint("etype in ('eq','lp','se','to','tr','vt','nt','qb','ce','ex',\
                               'sh','sn','th','av','co','df','ls','rb','rs','ve',\
                               'bc','mi','pc','ot','st','uk','lf','su','px')", name='EVENTTYPE02')

class Mec(Base):
    __tablename__ = "mec"

    mecid = Column(Integer, Sequence('mecseq'), primary_key=True, nullable=False)
    oridin = Column(ForeignKey('origin.orid'))
    oridout = Column(ForeignKey('origin.orid'))
    magid = Column(ForeignKey('netmag.magid'))
    commid = Column(Integer)
    mechtype = Column(String(2), info='Mechanism type')
    mecalgo = Column(String(15), info='This attribute represents the algorithm used to compute the mechanism (TDMT,TMTS=Dregers code; FPFIT=fpfit; SMTINV=Pasyanos code)')
    scalar = Column(Numeric, info='Seismic scalar moment (no value stored for fpfit')
    erscalar = Column(Numeric, info='Error in scalar moment. This is the standard deviation of the scalar moment')
    tft = Column(String(2), info='Type of source time function. Used to specify if time function and source half-duration are assumed, derived, or determined')
    tfd = Column(Numeric, info='Duration of the source time function')
    mxx = Column(Numeric, info='Moment tensor elements. Given in Aki convention')
    myy = Column(Numeric, info='Moment tensor elements. Given in Aki convention')
    mzz = Column(Numeric, info='Moment tensor elements. Given in Aki convention')
    mxy = Column(Numeric, info='Moment tensor elements. Given in Aki convention')
    mxz = Column(Numeric, info='Moment tensor elements. Given in Aki convention')
    myz = Column(Numeric, info='Moment tensor elements. Given in Aki convention')
    smxx = Column(Numeric, info='Uncertainties in moment tensor elements')
    smyy = Column(Numeric, info='Uncertainties in moment tensor elements')
    smzz = Column(Numeric, info='Uncertainties in moment tensor elements')
    smxy = Column(Numeric, info='Uncertainties in moment tensor elements')
    smxz = Column(Numeric, info='Uncertainties in moment tensor elements')
    smyz = Column(Numeric, info='Uncertainties in moment tensor elements')
    srcduration = Column(Numeric, info='Source half-duration')
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    strike1 = Column(Numeric, info='Strike of fault plane 1 from the best double couple')
    dip1 = Column(Numeric, info='Dip of fault plane 1 from the best double couple')
    rake1 = Column(Numeric, info='Rake of fault plane 1 from the best double couple')
    strike2 = Column(Numeric, info='Strike of fault plane 2 from the best double couple')
    dip2 = Column(Numeric, info='Dip of fault plane 2 from the best double couple')
    rake2 = Column(Numeric, info='Rake of fault plane 2 from the best double couple')
    unstrike1 = Column(Numeric, info='Uncertainty in strike 1')
    undip1 = Column(Numeric, info='Uncertainty in dip 1')
    unrake1 = Column(Numeric, info='Uncertainty in rake 1')
    unstrike2 = Column(Numeric, info='Uncertainty in strike 2')
    undip2 = Column(Numeric, info='Uncertainty in dip 2')
    unrake2 = Column(Numeric, info='Uncertainty in rake 2')
    eigenp = Column(Numeric, info='Eigen value of compressional axis in the principal axes representation of the best double couple')
    plungep = Column(Numeric, info='Plunge of compressional axis in the principal axes representation of the best double couple')
    strikep = Column(Numeric, info='Strike of compressional axis in the principal axes representation of the best double couple')
    eigenn = Column(Numeric, info='Eigen value of null axis axis in the principal axes representation of the best double couple')
    plungen = Column(Numeric, info='Plunge of null axis axis in the principal axes representation of the best double couple')
    striken = Column(Numeric, info='Strike of null axis axis in the principal axes representation of the best double couple')
    eigent = Column(Numeric, info='Eigen value of tension axis axis in the principal axes representation of the best double couple')
    plunget = Column(Numeric, info='Plunge of tension axis axis in the principal axes representation of the best double couple')
    striket = Column(Numeric, info='Strike of tension axis axis in the principal axes representation of the best double couple')
    nsta = Column(Integer, info='This quantity is the number of observations with non-zero input weights used to compute the mechanism. For CW=nsta, for FPFIT=nPobs, for HASH=nobs')
    pvr = Column(Numeric, info='Goodness of fit. For TDMT this is percent variance reduction; for FPFIT this is (1-misfit)*100')
    quality = Column(Numeric, info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    pdc = Column(Numeric, info='Percent double couple of the seismic moment tensor')
    pclvd = Column(Numeric, info='Percent compensated linear vector dipole of the seismic moment tensor')
    piso = Column(Numeric, info='Percent isotropic of the seismic moment tensor. If the solution is constrained to have zero trace (no "ISO"), then "ISO" should be NULL, as opposed to a case where the full moment tensor was used, and the ISO component ended up being very small ("0")')
    datetime = Column(Numeric, nullable=False, info='The date of associated with information in the record, in true epoch format')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('plungen >= 0 and plungen <= 90', name='mec13')
    CheckConstraint('plungep >= 0 and plungep <= 90', name='mec14')
    CheckConstraint('plunget >= 0 and plunget <= 90', name='mec15')
    CheckConstraint('pclvd >= 0 and pclvd <= 100', name='mec16')
    CheckConstraint('pdc >= 0 and pdc <= 100', name='mec17')
    CheckConstraint('piso >= 0 and piso <= 100', name='mec18')
    CheckConstraint('pvr >= 0 and pvr <= 100', name='mec19')
    CheckConstraint('rake1 >= -180 and rake1 <= 180', name='mec20')
    CheckConstraint('rake2 >= -180 and rake1 <= 180', name='mec21')
    CheckConstraint('srcduration >= 0.0 and srcduration <= 100', name='mec23')  #MTH: seems odd to have max srcduration!
    CheckConstraint('striken >= 0 and striken <= 360', name='mec24')
    CheckConstraint('strikep >= 0 and strikep <= 360', name='mec25')
    CheckConstraint('striket >= 0 and striket <= 360', name='mec26')
    CheckConstraint('strike1 >= 0 and strike1 <= 360', name='mec27')
    CheckConstraint('strike2 >= 0 and strike2 <= 360', name='mec28')
    CheckConstraint('tfd > 0', name='mec29')
    CheckConstraint('undip1 >= -180 and undip1 <= 180', name='mec30')
    CheckConstraint('undip2 >= -180 and undip2 <= 180', name='mec31')
    CheckConstraint('unrake1 >= -180 and unrake1 <= 180', name='mec38')
    CheckConstraint('unrake2 >= -180 and unrake2 <= 180', name='mec39')
    CheckConstraint('unstrike1 >= -180 and unstrike1 <= 180', name='mec40')
    CheckConstraint('unstrike2 >= -180 and unstrike2 <= 180', name='mec41')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='mec42')

    CheckConstraint('dip1 >= -90 and dip1 <= 90', name='mec01')
    CheckConstraint('dip2 >= -90 and dip2 <= 90', name='mec02')
    CheckConstraint('erscalar >= 0.0', name='mec03')
    CheckConstraint('mecid > 0', name='mec05')
    CheckConstraint("mechtype in ('FP', 'MT')", name='mec06')


class Netmag(Base):
    __tablename__ = "netmag"

    magid = Column(Integer, Sequence('magseq'), primary_key=True, nullable=False)
    orid = Column(ForeignKey('origin.orid'), nullable=False)
    commid = Column(Integer)
    magnitude = Column(Numeric, nullable=False, info='This gives the magnitude value of the type indicated by magtype')
    magtype = Column(String(6), nullable=False, info='This character string is used to specify the type of the magnitude measure')
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    magalgo = Column(String(15), info='This attribute represents the algorithm used to compute the magnitude')
    nsta = Column(Numeric, info='This quantity is the number of stations with non-zero input weights used to compute the magnitude')
    nobs = Column(Numeric, info='This quantity is the number of observations with non-zero input weights used to compute the magnitude')
    uncertainty = Column(Numeric, info='Magnitude uncertainty. Median of the absolute values of the channel magnitude residual deviations from the channel magnitudes median, the summary magnitude')
    gap = Column(Numeric, info='Gap in azimuthal coverage')
    distance = Column(Numeric, info='Distance from the origin to the nearest station')
    quality = Column(Numeric, info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been finalized')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('magnitude >= -10.0 and magnitude <= 10.0', name='netmag01')
    CheckConstraint("magtype in ('p','a','b','e','l','l1','l2','lg','c','s',\
                                 'w','z','B', 'un','d','h','n','dl')", name='netmag02')
    CheckConstraint('nsta >= 0', name='netmag03')
    CheckConstraint('uncertainty >= 0.0', name='netmag04')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='netmag05')
    CheckConstraint('magid > 0', name='netmag06')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='netmag07')
    CheckConstraint('nobs >= 0', name='netmag08')

class Origin(Base):
    __tablename__ = "origin"

    orid = Column(Integer, Sequence('orseq'), primary_key=True, nullable=False)
    evid = Column(ForeignKey('event.evid'), nullable=False)
    prefmag = Column(ForeignKey('netmag.magid'))
    prefmec = Column(ForeignKey('mec.mecid'))
    commid = Column(Integer)
    bogusflag = Column(Integer, nullable=False, info='This field describes whether an origin is bogus or not')
    datetime = Column(Numeric, nullable=False, info='The date of associated with information in the record, in true epoch format')
    lat = Column(Numeric, nullable=False, info='Locations north of the equator have positive latitudes')
    lon = Column(Numeric, nullable=False, info='Longitudes are measured positive east of the Greenwich meridian')
    depth = Column(Numeric, info='Distance in KM of the earthquake source below the geoid.')
    mdepth = Column(Numeric, info='Distance in KM of the earthquake source below the model surface.')
    type = Column(String(2), info='Type of location')
    algorithm = Column(String(15), info='Algorithm used to compute a seismic origin')
    algo_assoc = Column(String(80), info='Association algorithm used')
    auth = Column(String(15), nullable=False, info='The auth field specifies the source of the information')
    subsource = Column(String(8), info='A second identifier to specify the system or process that derived the data')
    datumhor = Column(String(8), info='Datum type for horizontal (latitude and longitude) coordinates')
    datumver = Column(String(8), info='Datum type for depth or elevation')
    gap = Column(Numeric, info='Gap in azimuthal coverage')
    distance = Column(Numeric, info='Distance from the origin to the nearest station')
    wrms = Column(Numeric, info='This attribute denotes the weighted RMS')
    stime = Column(Numeric, info='Origin time error. This attribute denotes the standard deviation of the origin time')
    erhor = Column(Numeric, info='Horizontal error. This attribute denotes the location uncertainty that accompanies the location')
    sdep = Column(Numeric, info='Depth error. This is the standard deviation of the depth estimate')
    erlat = Column(Numeric, info='Error in latitude. This is the standard deviation of the latitude estimate')
    erlon = Column(Numeric, info='Error in longitude. This is the standard deviation of the longitude estimate')
    totalarr = Column(Integer, info='Total number of phases in the database for the specified orid. This field signifies the number of P and S arrivals with non-zero input weights')
    totalamp = Column(Integer, info='Total number of amplitudes in the database for the specified orid. Not all amplitudes have to associated with all magnitudes or origins')
    ndef = Column(Integer, info='This attribute denotes the number of phases with non-zero output weights used in hypocenter calculation')
    nbs = Column(Integer, info='Number of S phase readings with non-zero output weights')
    nbfm = Column(Integer, info='Number of P first motions with non-zero input weights')
    locevid = Column(String(12), info='Local event identifier. This attribute describes the original event identifier of the event')
    quality = Column(Numeric, info='This attribute denotes the quality of an origin, an arrival, or a mechanism. 1.0 = good, 0.0 = bad')
    fdepth = Column(String(1), info='This attribute indicates if the depth was fixed or not to compute the solution')
    fepi = Column(String(1), info='This attribute indicates if the epicenter was fixed or not to compute the solution')
    ftime = Column(String(1), info='This attribute indicates if the origin time was fixed or not to compute the solution')
    vmodelid = Column(String(2), info='Velocity model identification. This string identifies a version (instance) of the velocity model for the specified domain and algorithm')
    cmodelid = Column(String(2), info='Velocity model domain. This string identifies the organization responsible for the velocity model')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    crust_type = Column(String(1), info='The type of crust model used for travel time calculations.')
    crust_model = Column(String(3), info='The code for the regional (geographic) crust model that is dominant in the travel time calculations.')
    gtype = Column(String(1), info='Geographic type (local, regional, teleseism)')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("datumhor in ('NAD27','WGS84')", name='origin02')
    CheckConstraint("datumver in ('NAD27','WGS84','AVERAGE')", name='origin03')
    CheckConstraint('depth >= -10.0 and depth <= 1000.0', name='origin04')
    CheckConstraint('distance >= 0.0', name='origin05')
    CheckConstraint('erhor >= 0.0', name='origin06')
    CheckConstraint('erlat >= 0.0', name='origin07')
    CheckConstraint('erlon >= 0.0', name='origin08')
    CheckConstraint("fdepth in ('y','n')", name='origin09')
    CheckConstraint("fepi in ('y','n')", name='origin10')
    CheckConstraint("ftime in ('y','n')", name='origin11')
    CheckConstraint('gap >= 0.0 and gap <= 360.0', name='origin12')
    CheckConstraint('nbfm >= 0', name='origin15')
    CheckConstraint('nbs >= 0', name='origin16')
    CheckConstraint('ndef >= 0', name='origin17')
    CheckConstraint('orid > 0', name='origin18')
    CheckConstraint('quality >= 0.0 and quality <= 1.0', name='origin19')
    CheckConstraint("type in ('H','h','C','c','A','a','D','d','u','U')", name='origin20')
    CheckConstraint('stime >= 0.0', name='origin21')
    CheckConstraint('wrms >= 0.0', name='origin23')
    CheckConstraint('sdep >=0.0', name='origin24')
    CheckConstraint('totalarr >=0', name='origin25')
    CheckConstraint('totalamp >= 0', name='origin26')
    CheckConstraint("rflag in ('a','h','f','A','H','F','i','I','c','C')", name='origin28')
    CheckConstraint("crust_type in ('H','T','E','L','V')", name='origin30')
    CheckConstraint("gtype in ('l','r','t')", name='origin31')
    #CheckConstraint("gtype in ('L','R','T')", name='origin31')

class Origin_error(Base):
    __tablename__ = "origin_error"

    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    sxx = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    syy = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    szz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    stt = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    sxy = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    sxz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    syz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    stx = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    sty = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    stz = Column(Numeric, info='Elements of the covariance matrix for the location defined by orid. The covariance matrix is symmetric (and positive definite) so that sxy = syx, etc., (x, y, z, t) refer to latitude, longitude, depth and origin time, respectively.')
    azismall = Column(Numeric, info='Azimuth of smallest principal error')
    dipsmall = Column(Numeric, info='Dip of smallest principal error')
    magsmall = Column(Numeric, info='Magnitude of smallest principal error')
    aziinter = Column(Numeric, info='Azimuth of intermediate principal error')
    dipinter = Column(Numeric, info='Dip of intermediate principal error')
    maginter = Column(Numeric, info='Magnitude of intermediate principal error')
    azilarge = Column(Numeric, info='Azimuth of largest principal error')
    diplarge = Column(Numeric, info='Dip of largest principal error')
    magslarge = Column(Numeric, info='Magnitude of largest principal error')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('aziinter >= 0.0 and aziinter <= 360.0', name='origin_error01')
    CheckConstraint('azilarge >= 0.0 and azilarge <= 360.0', name='origin_error02')
    CheckConstraint('azismall >= 0.0 and azismall <= 360.0', name='origin_error03')
    CheckConstraint('dipinter >= -90.0 and dipinter <= 90.0', name='origin_error04')
    CheckConstraint('diplarge >= -90.0 and diplarge <= 90.0', name='origin_error05')
    CheckConstraint('dipsmall >= -90.0 and dipsmall <= 90.0', name='origin_error06')
    CheckConstraint('maginter >= 0.0', name='origin_error07')
    CheckConstraint('maglarge >= 0.0', name='origin_error08')
    CheckConstraint('magsmall >= 0.0', name='origin_error09')
    CheckConstraint('stx > 0.0', name='origin_error10')
    CheckConstraint('sty > 0.0', name='origin_error11')
    CheckConstraint('stz > 0.0', name='origin_error12')
    CheckConstraint('sxx > 0.0', name='origin_error13')
    CheckConstraint('sxy > 0.0', name='origin_error14')
    CheckConstraint('sxz > 0.0', name='origin_error15')
    CheckConstraint('syy > 0.0', name='origin_error16')
    CheckConstraint('syz > 0.0', name='origin_error17')
    CheckConstraint('stt > 0.0', name='origin_error18')
    CheckConstraint('szz > 0.0', name='origin_error19')

class Remark(Base):
    __tablename__ = "remark"

    commid = Column(Integer, Sequence('commseq'), primary_key=True, nullable=False)
    lineno = Column(Integer, primary_key=True, nullable=False, info='Comment line number. This integer attribute is assigned as a sequence number for multiple line comments. The combination of commid and lineno is unique')
    remark = Column(String(80), info='Descriptive text. This single line of text is an arbitrary comment about a record in the database. The comment is linked to its parent relation only by forward reference from commid in the tuple of the relation of interest')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('commid > 0', name='remark01')
    CheckConstraint('lineno > 0', name='remark02')

class Remark_Origin(Base):
    __tablename__ = "remark origin"

    lineno = Column(ForeignKey('remark.lineno'), primary_key=True, nullable=False, info='Comment line number. This integer attribute is assigned as a sequence number for multiple line comments. The combination of commid and lineno is unique')
    commid = Column(ForeignKey('remark.commid'), primary_key=True, nullable=False, info='Comment Identifier')
    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False, info='Origin identification. Each origin is assigned a unique positive integer which identifies it in the database. The orid is used to identify one of the many hypotheses of the actual location of the event')
    lddate = Column(DateTime, server_default=text('NOW()'))

'''
Data associating amplitudes with magnitudes
'''
class Assocamm(Base):
    __tablename__ = "assocamm"

    magid = Column(ForeignKey('netmag.magid'), primary_key=True, nullable=False)
    ampid = Column(ForeignKey('amp.ampid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    weight = Column(Numeric, info='Magnitude weight')
    in_wgt = Column(Numeric, info='Input weight: 0.0-1.0')
    mag = Column(Numeric, info='Magnitude for this station reading')
    magres = Column(Numeric, info='Magnitude residual')
    magcorr = Column(Numeric, info='Magnitude correction for this channel')
    importance = Column(Numeric, info='This attribute denotes the importance of a phase/amplitude reading towards an origin/magnitude. 0.0 means no importance, 1.0 implies extremely important')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('mag >= -10.0 and mag <= 10.0', name='assocamm01')
    CheckConstraint('magcorr >= -10.0 and magcorr <= 10.0', name='assocamm02')
    CheckConstraint('magid > 0', name='assocamm03')
    CheckConstraint('weight >= 0.0 and weight <= 1.0', name='assocamm05')
    CheckConstraint('in_wgt >= 0.0 and in_wgt <= 1.0', name='assocamm06')
    CheckConstraint('importance > 0.0 and importance <= 1.0', name='assocamm07')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assocamm08')

'''
Data associating amplitudes with origins
'''
class Assocamo(Base):
    __tablename__ = "assocamo"

    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    ampid = Column(ForeignKey('amp.ampid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the seismic phase follows from source to receiver.')
    seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('seaz >= 0.0 and seaz <= 360.0', name='assocamo01')
    CheckConstraint('delta >= 0.0', name='assocamo02')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assocamo03')

'''
Data associating arrivals with origins
'''
class Assocaro(Base):
    __tablename__ = "assocaro"

    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    arid = Column(ForeignKey('arrival.arid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    iphase = Column(String(8))
    importance = Column(Numeric, info='This attribute denotes the importance of a phase/amplitude reading towards an origin/magnitude. 0.0 means no importance, 1.0 implies extremely important')
    delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the seismic phase follows from source to receiver.')
    seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')
    in_wgt = Column(Numeric, info='Input weight. 0.0-1.0')
    wgt = Column(Numeric, info='Location weight. This attribute gives the final weight assigned to the allied arrival by the location program.')
    timeres = Column(Numeric, info='Time residual. This attribute is a travel time residual, measured in seconds. The residual is found by taking the observed arrival time (saved in the Arrival relation) of a seismic phase and substracting the expected arrival time.')
    ema = Column(Numeric, info='Emergence angle residual. This attribute is the difference between an observed emergence angle and the theorical prediction for the same phase, assuming an event location as specified by the accompanying orid')
    slow = Column(Numeric, info='Slowness residual. This attribute gives the difference between an observed slowness (saved in the Arrival relation) and a theorical prediction')
    vmodelid = Column(Integer, info='Velocity model identification')
    scorr = Column(Numeric, info='Station correction')
    sdelay = Column(Numeric, info='Station delay')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    ccset = Column(String(1), info='This field describes whether the clock correction has been added or applied to the arrivals in computing a hypocenter solution')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('delta >= 0.0', name='assocaro02')
    CheckConstraint('ema >= 0 and ema <= 180', name='assocaro03')
    CheckConstraint('importance >= 0.0 and importance < 1.0', name='assocaro04')
    CheckConstraint('seaz >= 0.0 and seaz <= 360.0', name='assocaro05')
    CheckConstraint('slow >= 0.0', name='assocaro06')
    CheckConstraint('timeres >= 0.0', name='assocaro07')
    CheckConstraint('ccset < 1', name='assocaro09')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assocaro10')

'''
Data associating codas with magnitudes
'''
class Assoccom(Base):
    __tablename__ = "assoccom"
    magid = Column(ForeignKey('netmag.magid'), primary_key=True, nullable=False)
    coid = Column(ForeignKey('coda.coid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    weight = Column(Numeric, info='Magnitude weight')
    in_wgt = Column(Numeric, info='Input weight')
    mag = Column(Numeric, info='Magnitude for this station reading')
    magres = Column(Numeric, info='Magnitude residual')
    magcorr = Column(Numeric, info='Magnitude correction for this channel')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('weight >= 0.0 and weight <= 1.0', name='assoccomkey04')
    CheckConstraint('in_wgt >= 0.0 and in_wgt <= 1.0', name='assoccomkey05')
    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assoccomkey06')


'''
Data associating codas with origins
'''
class Assoccoo(Base):
    __tablename__ = "assoccoo"
    orid = Column(ForeignKey('origin.orid'), primary_key=True, nullable=False)
    coid = Column(ForeignKey('coda.coid'), primary_key=True, nullable=False)
    commid = Column(Integer)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8))
    delta = Column(Numeric, info='Source-receiver distance. This attribute is the arc length, over the earth�s surface, of the path the seismic phase follows from source to receiver.')
    seaz = Column(Numeric, info='Station to event azimuth. It is measured clockwise from North ')
    rflag = Column(String(2), info='This flag describes whether an observation was generated automatically, by a human, or has been saved, finalized or canceled')
    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint("rflag in ('a','h','f','A','H','F')", name='assoccookey04')

########################  Waveform Tables Below  ##################################
'''
Data associating waveforms with events
'''
class AssocWaE(Base):
    __tablename__ = "assocwae"
    wfid = Column(ForeignKey('waveform.wfid'), primary_key=True, nullable=False)
    evid = Column(ForeignKey('event.evid'), primary_key=True, nullable=False)
    datetime_on  = Column(Numeric, info='Time of first datum', nullable=False)
    datetime_off = Column(Numeric, info='Time of last datum', nullable=False)
    lddate = Column(DateTime, server_default=text('NOW()'))

'''
Summary information on a waveform filenames
'''
class Filename(Base):
    __tablename__ = "filename"

    fileid = Column(Integer, Sequence('fiseq'), primary_key=True, nullable=False)
    dfile  = Column(String(32), nullable=False,
                    info='Data file. This is the file name of a disk-based waveform file')
    datetime_on  = Column(Numeric, nullable=True, info='Time of first datum')
    datetime_off = Column(Numeric, nullable=True, info='Time of last datum')
    nbytes = Column(Numeric, nullable=True,
                    info='Number of bytes. This quantity is the number of bytes in a waveform segment or a file')
    subdirid = Column(ForeignKey('subdir.subdirid'),
                      info='Directory identifier. The key field is a unique identifier for a directory name on a disk')
    lddate = Column(DateTime, server_default=text('NOW()'))
    CheckConstraint('nbytes >= 0', name='fn01')

'''
Summary information on a directory name
'''
class Subdir(Base):
    __tablename__ = "subdir"

    subdirid = Column(Integer, Sequence('subseq'), primary_key=True, nullable=False)
    subdirname  = Column(String(64), nullable=False,
                    info='Path name. This is the name of a disk-based directory')
    lddate = Column(DateTime, server_default=text('NOW()'))

'''
Summary information on a waveform
'''
class Waveform(Base):
    __tablename__ = "waveform"

    wfid = Column(Integer, Sequence('waseq'), primary_key=True, nullable=False,
                  info='Waveform identifier. The key field is a unique identifier '\
                       'for a segment of digital waveform data')
    net  = Column(String(6), nullable=False)
    sta  = Column(String(6), nullable=False)
    auth = Column(String(15), nullable=False)
    subsource = Column(String(8), info='A second identifier to specify a unique source within the domain specified by auth')
    channel = Column(String(8), info='Channel name')
    channelsrc = Column(String(8), info='Domain for channel. This specifies what naming convention is used '\
                                        'for the channel name (i.e. SEED, USGS, etc.)')
    seedchan = Column(String(3), nullable=False,
                      info='SEED channel name. The first character denotes the band code, the second for the '\
                           'instrument code and the third represents the component code')
    location = Column(String(2), info='Describes the individual sites on an array station, operated by the '\
                                      'same network operator ')
    archive = Column(String(8), nullable=False,
                     info='This field specifies the place where the seismogram is archived')
    datetime_on  = Column(Numeric, nullable=False, info='Time of first datum')
    datetime_off = Column(Numeric, nullable=False, info='Time of last datum')
    samprate = Column(Numeric, nullable=False,
                       info='Sampling rate. This attribute is the sample rate in samples/second')
    wavetype = Column(String(1), info='This attribute denotes the type of waveform')

    fileid = Column(ForeignKey('filename.fileid'), nullable=False,
                      info='Filename identifier. The key field is a unique identifier for a physical file on a disk')

    foff = Column(Integer, info='File offset. This is the byte offset of a waveform segment within '\
                                'a data file. If the segment does not contain header information, '\
                                'this value is equal to traceoff')
    nbytes = Column(Integer, info='Number of bytes. This quantity is the number of bytes in a '\
                                      'waveform segment or a file. If the segment does not contain '\
                                      'header information, this value is equal to tracelen; otherwise '\
                                      'nbytes = headerlen + tracelen')
    traceoff = Column(Integer, info='Trace offset. This is the byte offset of the entire trace ')
    tracelen = Column(Integer, info='Number of bytes. This quantity represents the length of the entire trace')
    status = Column(String(1), nullable=False, info='This attribute denotes the status of the waveform in the file')
    wave_fmt = Column(Integer, info='Waveform type')
    format_id = Column(Integer, info='This attribute denotes a code indicating the encoding format. This number is assigned by the FDSN Data Exchange Working Group')
    wordorder = Column(Integer, info='Word order')
    recordsize = Column(Integer,
                        info='Record size for data. For MiniSEED, the value is the MiniSEED record size, '\
                             'eg. 512, 4096, etc')
    locevid = Column(String(12),
                     info='Local event identifier. This attribute describes the original '\
                          'event identifier of the event. In combination with auth and subsource, '\
                          'it can be used to uniquely identify the original event identifier from '\
                          'that source. For example, waveforms from the original source may be '\
                          'associated with this identifier')
    qc_level = Column(String(1),
                      info='This flag specifies the Data Header/Quality indicator flag for the specified waveform. '\
                           'Reference: SEED manual in Fixed Section of Data Header.' \
                           'For non-MiniSEED data, this would be the MiniSEED flag to be used '\
                           'if the data would be converted to MiniSEED')

    lddate = Column(DateTime, server_default=text('NOW()'))

    CheckConstraint('fileid > 0', name='wf01')
    CheckConstraint('foff >= 0', name='wf02')
    CheckConstraint('format_id >= 1', name='wf03')
    CheckConstraint('recordsize >= 0', name='wf04')
    CheckConstraint('samprate > 0', name='wf05')
    CheckConstraint('tracelen >= 0', name='wf06')
    CheckConstraint('traceoff >= 0', name='wf07')
    CheckConstraint("wavetype in ('C','T')", name='wf08')
    CheckConstraint('wave_fmt >= 1', name='wf09')
    CheckConstraint('wfid > 0', name='wf10')
    CheckConstraint('wordorder >= 0', name='wf11')
    CheckConstraint('nbytes >= 0', name='wf12')
    CheckConstraint("qc_level in ('R','D','Q','M')", name='wf13')
    CheckConstraint("status in ('E','T','A')", name='wf14')

'''
Summary information on a waveform
'''
class Waveroots(Base):
    __tablename__ = "waveroots"

    archive  = Column(ForeignKey('waveform.archive'), primary_key=True, nullable=False)
    wavetype = Column(ForeignKey('waveform.wavetype'), primary_key=True, nullable=False)
    status   = Column(ForeignKey('waveform.status'), primary_key=True, nullable=False)
    net      = Column(ForeignKey('waveform.net'), primary_key=True, nullable=False)
    wave_fmt = Column(ForeignKey('waveform.wave_fmt'), primary_key=True, nullable=False)
    datetime_on  = Column(ForeignKey('waveform.datetime_on'), primary_key=True, nullable=False)
    datetime_off = Column(ForeignKey('waveform.datetime_off'), primary_key=True, nullable=False)
    wcopy = Column(Integer, primary_key=True, nullable=False)
    fileroot = Column(String(256), nullable=False,
                      info='The top file directory of the waveform file. '\
                      'Subdirectories are allowed (i.e. /top_directory/subdirectory1/subdirectory2) '\
                      'but the top level must be included and it must be consistent with entries in SUBDIR')
#new_event = Event(auth='MH', etype='eq', version=0)
#new_event = Event(evid=70095793, auth='MH', etype='eq', version=0)
#new_event = Event(evid=-100, auth='MH', etype='eq')

#print(new_event.evid, new_event.auth)

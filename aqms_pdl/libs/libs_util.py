
import os
import sys
import yaml

from .. import installation_dir
from .libs_db import configure

from obspy.core.utcdatetime import UTCDateTime

import logging
logger = logging.getLogger()

def read_config(requireConfigFile=False):

    config = None

    # From highest to lowest priority location of config.yml:
    # 1. passed on cmd line --configFile=...
    # 2. os.getcwd() = Location script is called from
    # 3. INSTALL_DIR = Location of yasmine_cli directory
    # 4. None

    configfile = None
    # See if it was passed on cmd line:
    configfile = pick_off_configFile(sys.argv)

    if configfile is None:
        # Look for config.yml in dir where pdl-to-aqms is called from (changes)
        test_path = os.path.join(os.getcwd(), 'config.yml')
        if os.path.exists(test_path):
            configfile = test_path
    if configfile is None:
        # Look for config.yml in dir where pdl_to_aqms.py is located (it never changes)
        #SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
        #test_path = os.path.join(SCRIPT_DIR, 'config.yml')
        INSTALL_DIR = installation_dir()
        test_path = os.path.join(INSTALL_DIR, 'config.yml')
        if os.path.exists(test_path):
            configfile = test_path

    if configfile:
        config = configure(configfile)
    else:
        if requireConfigFile:
            logger.ERROR("ConfigFile is required but none has been found --> Exit")
            exit(2)

    return config

def pick_off_configFile(argv):
    configFile= None
    for word in argv:
        if '--configFile' in word:
            foo, configFile = word.split('--configFile=')
    return configFile

# Either will work
# import xml.etree.ElementTree as ET
from lxml import etree as ET
import shutil

def archive_quakeml(args):

    ns = "{http://quakeml.org/xmlns/bed/1.2}"

    if 'archive_quakeml_dir' not in args:
        logger.error("archive_quakeml_dir must be set in config.yml!")
        return
    if not os.path.isdir(args.archive_quakeml_dir):
        logger.error("archive_quakeml_dir:%s is not a dir!" % args.archive_quakeml_dir)
        return
    if not isWritable(args.archive_quakeml_dir):
        logger.error("archive_quakeml_dir:%s is not writable!" % args.archive_quakeml_dir)
        return

    quakemlfile = os.path.join(args.directory, 'quakeml.xml')

    # Get the eventParameters/creationInfo/creationTime from the quakemlfile
    #     and convert to timestamp_str
    with open(quakemlfile,"r") as fp:
        element = ET.parse(fp)
        e = element.findall('{0}eventParameters/{0}creationInfo/{0}creationTime'.format(ns))
    if e:
        UTC = UTCDateTime(e[0].text)
        # Convert timestamp to millisecs and then to string
        timestamp = float(UTC.timestamp) * 1000
        timestamp_str = "%d" % timestamp
    else:
        logger.error("The quakeml file:%s does not contain an eventParameters/creationInfo/creationTime!"
                     % quakemlfile)
        exit(2)

    # Desired archive on disk:
    # archive_dir   code     timestamp
    # archive_dir/us70008j5/1582562692040/run.sh
    # archive_dir/us70008j5/1582562692040/quakeml.xml
    # archive_dir/us70008j5/1582562696040/run.sh
    # archive_dir/us70008j5/1582562696040/quakeml.xml

    archive_dir = args.archive_quakeml_dir
    code_dir = os.path.join(archive_dir, args.code)

    if not os.path.isdir(code_dir):
        try:
            os.mkdir(code_dir)
        except OSError as exc:
            logger.error("Unable to create code_dir:%s returned:%s" % (code_dir, exc))
            return
    else:
        logger.debug("code_dir:%s already exists" % code_dir)

    timestamp_dir = os.path.join(code_dir, timestamp_str)
    if not os.path.isdir(timestamp_dir):
        try:
            os.mkdir(timestamp_dir)
        except OSError as exc:
            logger.error("Unable to create dir:%s returned:%s" % (timestamp_dir, exc))
            return

    if args.directory == timestamp_dir:
        #logger.info("This seems to be running from an archive script: args.directory==archive_quakeml_dir:%s --> don't rearchive" % args.directory)
        logger.info("This seems to be running from an archive script --> Don't rearchive")
        return


    # Copy the PDL quakeml.xml file into the timestamp_dir:
    dest = shutil.copy(quakemlfile, timestamp_dir)

    # Create the run string and save to run.sh exectuable in timestamp_dir:
    args = []
    # Remove --archive_quakeml flags and redirect --directory to archive_dir
    for arg in sys.argv:
        if '--directory' in arg:
            args.append('--directory=%s' % timestamp_dir)
        elif '--archive-quakeml' in arg:
            pass
        else:
            args.append(arg)

    run_string = "#!/bin/sh\n"
    run_string += "%s   \\" % args[0]
    i=1
    while i < len(args) - 1:
        run_string += "\n    %s    \\" % args[i]
        i += 1
    run_string += "\n    %s" % args[i]

    script_file = os.path.join(timestamp_dir, 'run.sh')
    with open(script_file, 'w') as fp:
        fp.write(run_string)

    os.chmod(script_file, 0o775)

    script_file = os.path.join(timestamp_dir, 'run.sh')
    logger.info(" PDL quakeml archived at:%s" % os.path.join(timestamp_dir, 'quakeml.xml'))
    logger.info("To rerun PDL message see:%s" % script_file)

    return


def isWritable(directory):
    try:
        tmp_prefix = "write_tester";
        count = 0
        filename = os.path.join(directory, tmp_prefix)
        while(os.path.exists(filename)):
            filename = "{}.{}".format(os.path.join(directory, tmp_prefix),count)
            count = count + 1
        f = open(filename,"w")
        f.close()
        os.remove(filename)
        return True
    except Exception as e:
        #print "{}".format(e)
        return False

'''
def configure(filename=None):
    """
        Read in config from yaml filename
    """
    configuration = {}
    if not os.path.exists(filename):
        logger.error("configFile=[%s] does not exist --> Exit" % filename)
        exit(2)

    with open(filename, 'r') as ymlfile:
        configuration = yaml.load(ymlfile, Loader=yaml.FullLoader)
    return configuration
'''


import sqlite3

sqlite3_db = None
def init_sqlitedb(fname):
    global sqlite3_db
    if sqlite3_db is None:
        sqlite3_db = fname
    conn = sqlite3.connect(fname)
    conn.execute("""CREATE TABLE IF NOT EXISTS processed_msgs (
                    hash TEXT,
                    hash_string TEXT
                )""")
    conn.close()

import hashlib
def get_hash(obspy_object, code):

    conn = sqlite3.connect(sqlite3_db)
    name = type(obspy_object).__name__
    hash_string = "%s.%s" % (code, name)

    if obspy_object.creation_info:
        info = obspy_object.creation_info
        if info.creation_time:
            hash_string += ".%s" % info.creation_time
        if info.version:
            hash_string += ".%s" % info.version

    hash_object = hashlib.md5(hash_string.encode())
    #print(hash_object.hexdigest())

    values = (hash_object.hexdigest(), hash_string)
    #print(values)
    cur = conn.execute("""SELECT * FROM processed_msgs where hash='%s'""" % hash_object.hexdigest())
    rows = cur.fetchall()
    insert = True
    if rows:
        #print("Found match --> IGNORE")
        insert = False
    #else:
        #print("No match --> INSERT")

    cur.close()
    conn.close()

    return insert

def put_hash(obspy_object, code):
    conn = sqlite3.connect(sqlite3_db)

    name = type(obspy_object).__name__
    hash_string = "%s.%s" % (code, name)

    if obspy_object.creation_info:
        info = obspy_object.creation_info
        if info.creation_time:
            hash_string += ".%s" % info.creation_time
        if info.version:
            hash_string += ".%s" % info.version

    hash_object = hashlib.md5(hash_string.encode())
    values = (hash_object.hexdigest(), hash_string)
    conn.execute("""INSERT INTO processed_msgs VALUES (?, ?)""", values)
    conn.commit()
    conn.close()

    return values


if __name__ == "__main__":
    main()

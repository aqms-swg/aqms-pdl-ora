#!/Users/mth/mth/miniconda3/bin/python

import argparse
import os
import io
import sys
from obspy.core.event import read_events

def main():

    quakemlfile = processCmdLine()
    print("Scan quakemlfile:%s" % quakemlfile)
    summary = scan_quakeml(quakemlfile)
    print(summary)
    '''
    if summary[-1] == '\n':
        print("This has trailing return")
    else:
        print("This has NO trailing return")
    '''

def scan_quakeml(quakemlfile=None):

    event = None
    mag = None
    origin = None
    preferred_origin = False
    # Read quakeml into string and replace anssevent tags:
    with open(quakemlfile, 'r') as file:
        xml = file.read().replace('anssevent:internalEvent', 'event')
    try:
        #cat = read_events(quakemlfile, format="quakeml")
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
        event = cat[0]
    except:
        raise

    names = {"Catalog":"Cat",
             "Event":"Evt",
             "Origin":"Org",
             "Magnitude":"Mag",
             "FocalMechanism":"Mec",
            }

    string = "quakeml summary: [nPicks:%d nAmps:%d nOrigins:%d nMags:%d nFocalMechs:%s]\n" % \
            (len(event.picks), len(event.amplitudes), len(event.origins), len(event.magnitudes), len(event.focal_mechanisms))
    string += ("%-31s %s %-54s %18s %16s %s\n" % ("         [creation_time]", "[agency]", "     [resource_id]", "[data_id]", "[event_id]", "[P]"))
    string +=("-"*135)
    string += "\n"

    objs = [cat, event] + event.origins + event.magnitudes + event.focal_mechanisms
    for obj in objs:
        cls = type(obj).__name__
        cls_name = "Unk"
        if cls in names:
            cls_name = names[cls]

        dsource = "N/A"
        dataid = "N/A"
        esource = "N/A"
        eventid = "N/A"
        if hasattr(obj, 'extra'):
            if 'datasource' in obj.extra:
                dsource = obj.extra['datasource']['value']
            if 'dataid' in obj.extra:
                dataid = obj.extra['dataid']['value']
            if 'eventsource' in obj.extra:
                esource = obj.extra['eventsource']['value']
            if 'eventid' in obj.extra:
                eventid = obj.extra['eventid']['value']

        #eventid="us_70007v9g_mww"
        creation_time = "N/A"
        agency_id = "--"
        if obj.creation_info:
            creation_time = obj.creation_info.creation_time
            agency_id = obj.creation_info.agency_id

        pref = ""
        flags = None
        if cls == "Origin":
            if event.preferred_origin() and obj.resource_id.id == event.preferred_origin().resource_id.id:
                flags = "*"
            if event.focal_mechanisms:
                fm = event.preferred_focal_mechanism() if event.preferred_focal_mechanism() else event.focal_mechanisms[0]
                if fm.triggering_origin_id and obj.resource_id == fm.triggering_origin_id:
                    if flags:
                        flags += "T"
                    else:
                        flags = "T"

        elif cls == "Magnitude":
            if event.preferred_magnitude() and obj.resource_id.id == event.preferred_magnitude().resource_id.id:
                flags = "*"
        elif cls == "FocalMechanism":
            if event.preferred_focal_mechanism() and obj.resource_id.id == event.preferred_focal_mechanism().resource_id.id:
                flags = "*"

        if flags:
            string += ("%3s: %-28s %1s%2s%1s %-56s %18s %16s [%s]\n" % \
                (cls_name, creation_time, " ",agency_id, " ", obj.resource_id.id, dataid, eventid, flags))
        else:
            string += ("%3s: %-28s %1s%2s%1s %-56s %18s %16s\n" % \
                (cls_name, creation_time, " ",agency_id, " ", obj.resource_id.id, dataid, eventid))

        #if cls == "Event":
            #string += ("%-38s [nPicks:%d nAmps:%d nOrigins:%d nMags:%d nFocalMechs:%d]\n" % \
                #("     -contd-", len(obj.picks), len(obj.amplitudes), len(obj.origins), len(obj.magnitudes), len(obj.focal_mechanisms)))
        if cls == "Origin":
            string += (" OT: %-28s loc: <%5.2f,%7.2f> [h=%.2f km] nArr:%d [mode:%s] [status:%s]\n" % \
                (obj.time, obj.latitude, obj.longitude, obj.depth/1e3, len(obj.arrivals), obj.evaluation_mode, obj.evaluation_status))
        elif cls == "Magnitude":
            string += ("%-38s [%s: %.2f] [mode:%s] [status:%s]\n" % \
                ("     -contd-",obj.magnitude_type, obj.mag, obj.evaluation_mode, obj.evaluation_status))

    return string

def processCmdLine():

    parser = argparse.ArgumentParser()
    optional = parser._action_groups.pop()
    required = parser.add_argument_group("required arguments")


    required.add_argument("--quakeml", type=str, metavar='Path to quakeml.xml file', required=True)

    #parser._action_groups.append(optional) # 
    #optional.add_argument("--optional_arg")

    args, unknown = parser.parse_known_args()

    return args.quakeml


if __name__ == "__main__":
    main()

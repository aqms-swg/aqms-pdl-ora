-- run as user TRINETDB
CREATE TABLE ASSOCEVENTS
 (      EVID NUMBER(20) NOT NULL ,
        EVIDASSOC NUMBER(20) NOT NULL ,
        COMMID NUMBER(20),
        LDDATE DATE DEFAULT (cast (sys_extract_utc(systimestamp) as date)),
        CONSTRAINT ASSOCEVENTS_PK PRIMARY KEY (EVID, EVIDASSOC)
 );
 
create or replace public synonym ASSOCEVENTS for ASSOCEVENTS;
create unique index unique_combinations on ASSOCEVENTS (least(evid, evidassoc), greatest(evid, evidassoc));
grant select on ASSOCEVENTS to trinetdb_read, code;
grant insert,update,delete on ASSOCEVENTS to trinetdb_write, code;

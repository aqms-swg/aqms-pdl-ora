import io
import numpy as np
import os
import sys

from obspy.core.event import read_events
from obspy.core.utcdatetime import UTCDateTime
from obspy.geodetics.base import gps2dist_azimuth
from .libs.schema_new import Eventprefmag, Eventprefor, Eventprefmec, Origin, Event
from .libs.schema_new import Assocevents, Remark, Epochtimebase
from sqlalchemy import exc

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

from . import installation_dir

from .libs.libs_log import configure_logger
from .libs.libs_cmd import process_cmd_line
from .libs.libs_util import read_config, archive_quakeml
#from .libs.libs_util import get_hash, put_hash, init_sqlitedb

from .libs.libs_aqms import (obspy_event_to_aqms, obspy_origin_to_aqms, obspy_magnitude_to_aqms,
                            obspy_arrival_to_aqms, obspy_amplitude_to_aqms,
                            obspy_amplitude_to_assocamm, obspy_amplitude_to_assocamo,
                            obspy_arrival_to_assocaro, obspy_focal_mechanism_to_aqms,
                            aqmsprefor, aqmsprefmag, aqmsprefmec, summarize)

from .libs.libs_amps import scan_xml_amps, unassoc_amplitude_to_aqms
from .libs.libs_db import test_connection, configure
from .libs.libs_obs import scan_quakeml

KM_PER_DEG = 111.19

test_eventids = [ 'usauto400063yy', 'us_400063yy',
                  'us_400063yy_mww', 'us_a000es01_mww',
                  'nc73554385_mt1']

fname = 'pdl_to_aqms'

def main():

    config = read_config()
    configure_logger(config, logfile="%s.log" % fname, levelString=None)
    quakemlfile, args = process_cmd_line(fname, config)

    #for eventid in test_eventids:
        #pdl_prefix, pdl_id = parse_eventid(eventid)
        #print("eventid:%s prefix:%s pdl_id:%s" % (eventid, pdl_prefix, pdl_id))
    #exit()

    #auth = args.preferred_eventsource.upper()     # 'us' --> 'US'
    #pdl_id = args.preferred_eventsourcecode    #  pdl_id gets stored in db as origin.locevid
    RSN_source = config['RSN_source']

    logger.info("main: RSN_source:[%s]" % RSN_source)

    auth   = args.source.upper()
    pdl_id, RSN_pdl_id, other_pdl_ids = check_eventids(args, RSN_source, keep_them_separated=True)

    if len(pdl_id) > 12:
        new_id = pdl_id[:12]
        logger.info("pdl_id=%s has len > 12 ==> shorten to:%s for storage in origin.locevid" %
                    (pdl_id, new_id))
        pdl_id = new_id

    pdl_type = args.type          #  PDL message type, e.g., 'origin', 'phase_data' or 'moment-tensor'
    subsource = 'PDL'
    if args.subsource:
        logger.info("%s: Setting subsource=%s" % (fname, args.subsource))
        subsource=args.subsource

    if pdl_id is None:
        logger.error("%s: Must pass --preferred-eventsourcecode=... to set pdl_id ==> Exitting!" %
                     fname)
        exit(2)

    # At this point, internal-moment-tensor will have auth=USAUTO + locevid=400063yy
    #       while subsequent origin message will have auth=US     + locevid=6000diwi

    if test_connection(config):
        logger.info("%s: We have a database connection!" % fname)
        createSession(config)
    else:
        logger.error("%s: No database connection ==> Stop!" % fname)
        exit(2)

    if pdl_type == 'unassociated-amplitude':
        handle_unassociated_amp(quakemlfile, use_db)
        logger.info("%s: Handle unassociated-amplitude message and exit" % fname)
        exit(0)

    if pdl_type == 'internal-moment-tensor':
        logger.info("%s: pdl_type = internal-moment-tensor --> Expect Mww magnitude" % (fname))

    selectflag = 1
    if args.set_selectflag_zero or ('set_selectflag_zero' in config and config['set_selectflag_zero']==True):
        logger.info("Setting selectflag=0 for PDL events !!")
        selectflag = 0


    assoc_thresh = thresh(time=2., km=10., depth=20.)

    if 'assoc_thresh' in config and 'time' in config['assoc_thresh'] and 'km_dist' in config['assoc_thresh'] \
       and 'km_depth' in config['assoc_thresh']:
        assoc_thresh = thresh(time=config['assoc_thresh']['time'],
                              km=config['assoc_thresh']['km_dist'],
                              depth=config['assoc_thresh']['km_depth'])

    do_association = True if 'associate' in config and config['associate'] else False

    found_PDL_orig = None
    if do_association:
        logger.info("%s: Use assoc_thresh: time:%.1f, dist:%.1f km depth:%.1f km" %
                    (fname, assoc_thresh.time, assoc_thresh.km, assoc_thresh.depth))

        # Is this pdl_id already in the db (as origin.locevid) ?
        #found_PDL_orig = query_for_pdl_event_in_aqms('00804187')
        found_PDL_orig = query_for_pdl_event_in_aqms(pdl_id)

        if found_PDL_orig is None:
            # Try the other ids pulled from eventids
            for other_id in other_pdl_ids:
                found_PDL_orig = query_for_pdl_event_in_aqms(other_id)
                if found_PDL_orig:
                    break
        if found_PDL_orig:
            logger.info("%s: Found match pdl_id:%s evid:%s orid:%s in local db" %
                        (fname, found_PDL_orig.locevid, found_PDL_orig.evid, found_PDL_orig.orid))
        else:
            logger.info("%s: No matching/related pdl_ids found in db" % (fname))

    if args.archive_quakeml:
        archive_quakeml(args)

    logger.info(scan_quakeml(quakemlfile))

# 1. Read in the PDL quakeml file:
    event, origin, magnitude, focal_mechanism = read_quakemlfile(quakemlfile)

    min_dist = None
    # Can origin be None ???
    if origin.arrivals:
        arrivals = []
        # MTH: Test for bad arrivals with no distance set
        for arr in origin.arrivals:
            if not getattr(arr, 'distance', None) or \
               arr.distance <= 0:
                logger.warning("quakemlfile=[%s] Got arrival with bad distance:%s --> Drop arrival!" %
                               (quakemlfile, arr.distance))
            else:
                arrivals.append(arr)

        origin.arrivals = arrivals

    if origin.arrivals:
        sorted_arrivals = sorted(origin.arrivals, key=lambda x: x.distance, reverse=False)
        #MTH: obspy distance is in deg and all AQMS lengths are km:
        min_dist = sorted_arrivals[0].distance * KM_PER_DEG

    if args.debug == 'summarize-only':
        summarize(event, origin, magnitude)
        exit(0)

    # Add leap seconds to origin.time and any event.picks:
    fix_leap_seconds(origin, event)

    insert_origin = True
    insert_event = True
    insert_magnitude = False
    insert_mechanism = False
    evid = None
    orid = None
    magid = None
    mecid = None

    if found_PDL_orig:
        insert_event = False
        insert_origin = False
        evid = found_PDL_orig.evid
        logger.info("%s: found_PDL_orig [orid=%s] has evid=%s --> Use this evid" %
                    (fname, found_PDL_orig.orid, found_PDL_orig.evid))
        if pdl_id == found_PDL_orig.locevid:
            orid = found_PDL_orig.orid
            logger.info("%s: found_PDL_orig [orid=%s] locevid=%s matches pdl_id=%s --> compare origins" %
                        (fname, found_PDL_orig.orid, found_PDL_orig.locevid, pdl_id))
            if origin:
                origin_matches = compare_origins(quakeml_origin=origin, aqms_origin=found_PDL_orig)
                if not origin_matches:
                    logger.info("%s: This origin does not match found_PDL_orig [orid=%s] --> update origin" %
                                (fname, found_PDL_orig.orid))
                    insert_origin = True
                    orid = getSequence('origin', 1)[0]
                    logger.info("%s: Request new orid from sequence returned: [orid=%s]" % (fname, orid))
        else:
            logger.info("%s: found_PDL_orig [orid=%s] locevid=%s DOES NOT MATCH pdl_id=%s --> insert new origin" %
                        (fname, found_PDL_orig.orid, found_PDL_orig.locevid, pdl_id))
            insert_origin = True
            orid = getSequence('origin', 1)[0]
            logger.info("%s: Request new orid from sequence returned: [orid=%s]" % (fname, orid))
    else:
        logger.info("%s: pdl_id:%s not found in local db --> Create new evid + orid" %
                    (fname, pdl_id))
        evid = getSequence('event', 1)[0]
        orid = getSequence('origin', 1)[0]
        logger.info("%s: Request new evid from sequence returned: [evid=%s]" % (fname, evid))
        logger.info("%s: Request new orid from sequence returned: [orid=%s]" % (fname, orid))

    if origin:
        logger.info("quakeml file contains [%d] arrivals [%d] amplitudes [%d] picks" % \
                    (len(origin.arrivals), len(event.amplitudes), len(event.picks)))
        insert_arrivals = True if (len(origin.arrivals) > 0) and not args.ignore_arrivals else False
        insert_amplitudes = True if (len(event.amplitudes) > 0) and not args.ignore_amplitudes else False

# 3. Prefetch all ids needed for insertion
#    we should already have evid + orid

    if magnitude:
        magid = getSequence('magnitude', 1)[0]
        logger.info("%s: Request magid returned:%s" % (fname, magid))
        insert_magnitude = True
    if focal_mechanism:
        mecid = getSequence('mechanism', 1)[0]
        logger.info("%s: Request mecid returned:%s" % (fname, mecid))
        insert_mechanism = True
    if insert_arrivals:
        arids = getSequence('arrival', len(origin.arrivals))
    if insert_amplitudes:
        ampids = getSequence('amplitude', len(event.amplitudes))

    session = Session()


# 4. Convert obspy objects to AQMS table equivalents and add to session:
    if insert_event:
        aqms_event= obspy_event_to_aqms(event, orid=orid, evid=evid, magid=magid,
                                        mecid=mecid, auth=auth, subsource=subsource, selectflag=selectflag)
        #if getattr(args, 'event_remark', None):
        if 'event_remark' in config:
            #rmk = Remark(lineno=1, remark=args.event_remark[:79])
            rmk = Remark(lineno=1, remark=config['event_remark'])
            session.add(rmk)
            session.flush()
            aqms_event.commid = rmk.commid
            logger.info("Add commid:%d rmk:%s" % (rmk.commid, rmk.remark))
        session.add(aqms_event)
    else:
        aqms_event = session.query(Event).filter(Event.evid==evid).all()[0]

    if insert_origin:
        aqms_orig = obspy_origin_to_aqms(event, orid=orid, evid=evid, magid=magid,
                                         mecid=mecid, auth=auth, pdl_id=pdl_id,
                                         subsource=subsource, min_dist=min_dist)
        gtype = set_gtype(config, RSN_source, aqms_orig.lat, aqms_orig.lon)
        if gtype is not None:
            aqms_orig.gtype = gtype
        else:
            logger.info("set_gtype() returned None --> origin.gtype not set")

        session.add(aqms_orig)
        session.flush()
        aqms_event.prefor = orid
        prefor = session.query(Eventprefor).filter(Eventprefor.evid==evid).one_or_none()
        if prefor:
            logger.info("Update eventprefor evid:%s old_orid:%s --> new_orid:%s" % (evid, prefor.orid, orid))
            prefor.orid = orid
        else:
            logger.info("Unable to locate evid=%s in eventprefor table --> make new entry with orid=%s" % 
                         (evid, orid))
            session.add(aqmsprefor(evid, orid))
    else:
        aqms_orig = found_PDL_orig


    if do_association:
        # See if this event associates with any non-PDL event(s) in the db
        # MTH: Keep source/auth with the various pdl_ids for uniqueness ?

        # 1. If an RSN-looking evid is passed from PDL --eventids=[..], see if the evid
        #    is in the db and add row to assocevents
        #    This would only happen if RSN pushed the event through PDL and another
        #    non-RSN event message came back through PDL with the RSN evid (prefixed by RSN auth)

        if RSN_pdl_id:
            # This should be a direct query on Event, not locevid
            RSN_evid = None
            RSN_orig = None
            try:
                RSN_evid = int(RSN_pdl_id)
                RSN_orig = query_evid(RSN_evid, session=session)
            except:
                logger.warning("RSN evid=%s cannot be read as int --> Can't use to associate with PDL evids" % RSN_pdl_id)

            if RSN_orig:
                logger.info("Assocevents [pdl_id] RSN_pdl_id=%s was found in RSN db evid:%d orid:%d" %
                            (RSN_pdl_id, RSN_orig.evid, RSN_orig.orid))
                comment = 'PDL --eventids:'
                comment += ' '.join(args.eventids)
                if query_assocevents(RSN_orig.evid, aqms_orig.evid, session=session):
                    logger.info("Assocevents(%d, %d) already in Table --> Do nothing" % (RSN_orig.evid, aqms_orig.evid))
                else:
                    logger.info("Add assocevents(%d, %d)" % (RSN_orig.evid, aqms_orig.evid))
                    #commid = getSequence('remark', 1)[0]
                    #logger.info("getSequence for remark returned commid=%s" % (commid))
                    commid = None
                    if comment:
                        #rmk = Remark(commid=commid, lineno=1, remark=comment)
                        rmk = Remark(lineno=1, remark=comment[:79])
                        session.add(rmk)
                        session.flush()
                        commid = rmk.commid
                        logger.info("Add commid=%d remark=%s" % (rmk.commid, comment))
                    session.add(Assocevents(evid=RSN_evid, evidassoc=evid, commid=commid))
            else:
                logger.info("RSN_pdl_id=%s was not found in RSN db origin.evid fields" % RSN_pdl_id)


        # 2. Look through the db for non-PDL origins within assoc_thresh of this PDL origin
        #    If any are close, insert new assocevents row

        # Return list of non-PDL event(s) with origins that associate
        local_origins = associate_local_origin(aqms_orig, RSN_source, assoc_thresh)

        if local_origins:
            for local_orig in local_origins:
                logger.info("Assocevents [assoc_thresh] RSN local_orig.evid=%d orid:%d associates within assoc_thresh" %
                            (local_orig.evid, local_orig.orid))
                od = calc_origin_diff(local_orig, aqms_orig)
                comment = 'orids:%d and %d are close: dt:%.3f s dx:%.2f km dz:%.2f km' % \
                            (local_orig.orid, aqms_orig.orid, od.dt, od.dx, od.dz)
                if query_assocevents(local_orig.evid, aqms_orig.evid, session=session):
                    logger.info("Assocevents(%d, %d) already in Table --> Do nothing" % (local_orig.evid, aqms_orig.evid))
                else:
                    logger.info("Add assocevents(%d, %d)" % (local_orig.evid, aqms_orig.evid))
                    commid = None
                    if comment:
                        rmk = Remark(lineno=1, remark=comment[:79])
                        session.add(rmk)
                        session.flush()
                        commid = rmk.commid
                        logger.info("Add commid=%d remark=%s" % (rmk.commid, comment))
                    session.add(Assocevents(evid=local_orig.evid, evidassoc=aqms_orig.evid, commid=commid))


    # MTH: Need better mapping between expected PDL magtype and allowable AQMS magtype
    # aqms magtype in ('p','a','b','e','l','l1','l2','lg','c','s','w','z','B', 'un','d','h','n','dl')
    if insert_magnitude:
        magtype = magnitude.magnitude_type.lower()[-1] # ML --> 'l'  Mww --> 'w'
        # Mwr, etc are not valid types
        #if magnitude.magnitude_type[0:2].lower() == 'Mw' and magnitude.magnitude_type[2] != 'w':
        if magnitude.magnitude_type[0:2].lower() == 'mw':
            logger.info("magnitude type=[%s] --> inserting as aqms netmag type='w' " % magnitude.magnitude_type)
            magtype = 'w'

        aqms_mag  = obspy_magnitude_to_aqms(magnitude, orid=orid, evid=evid, magid=magid, auth=auth,
                                            magtype=magtype, subsource=subsource, min_dist=min_dist)
        # This should distinguish between Mww, Mwr, Mwb, etc when they all have magtype = 'w'
        aqms_mag.magalgo = magnitude.magnitude_type
        logger.info("Incoming magnitude_type=[%s] --> Set magtype=%s magalgo=%s" %
                    (magnitude.magnitude_type, aqms_mag.magtype, aqms_mag.magalgo))

        session.add(aqms_mag)
        session.flush()
        #session.add(aqmsprefmag(evid, magid, magtype=aqms_mag.magtype))

    if insert_mechanism:
        #TODO: MTH: For moment-tensor msg with no preferred mag, need to find the associated magid
        #           before next step. For now set to None
        aqms_mec = obspy_focal_mechanism_to_aqms(focal_mechanism, event, orid=orid, magid=magid,
                                                 mecid=mecid, auth=auth, subsource=subsource)
        session.add(aqms_mec)
        session.flush()
        prefmec = session.query(Eventprefmec).filter( Eventprefmec.evid==evid,
                                                      Eventprefmec.mechtype==aqms_mec.mechtype
                                                    ).one_or_none()
        if prefmec:
            logger.info("Update eventprefmec evid:%s type:%s old_mecid:%s --> new:%s" % \
                        (evid, prefmec.mechtype, prefmec.mecid, mecid))
            prefmec.mecid = mecid
        else:
            logger.info("Unable to locate evid:%s type:%s in eventprefmec table --> insert new mecid:%s" % \
                        (evid, aqms_mec.mechtype, mecid))
            session.add(aqmsprefmec(evid, mecid, mechtype=aqms_mec.mechtype))


    if insert_arrivals:
        aqms_arrivals = []
        assocaro_adds = []

        for arrival in origin.arrivals:
            arid = arids.pop(0)
            # agency_id: 'pr'  ?? use this ??
            auth_arr = auth
            if getattr(arrival, 'creation_info', None) and \
            getattr(arrival.creation_info, 'agency_id', None):
                auth_arr = arrival.creation_info.agency_id.upper()
            else:
                logger.debug("Arrival doesn't have creation_info --> use origin auth=[%s] for auth_arr" % (auth))

            logger.debug("Convert arrival: arid=[%s] auth=[%s]" % (arid, auth))
            #aqms_arrivals.append( obspy_arrival_to_aqms(arrival, arid=arid, auth=auth, subsource=subsource) )
            #assocaro_adds.append( obspy_arrival_to_assocaro(arrival, arid=arid, orid=orid, auth=auth, subsource=subsource) )
            aqms_arr = obspy_arrival_to_aqms(arrival, arid=arid, auth=auth, subsource=subsource)
            if aqms_arr:
                aqms_arrivals.append(aqms_arr)
                # assocaro - Data associating arrivals with origins
                assocaro_adds.append( obspy_arrival_to_assocaro(arrival, arid=arid, orid=orid, auth=auth, subsource=subsource) )
        if aqms_arrivals:
            session.bulk_save_objects(aqms_arrivals)
            logger.info("Insert %d arrivals arid:%d -to- %d" % (len(aqms_arrivals), aqms_arrivals[0].arid, aqms_arrivals[-1].arid))
        else:
            logger.warning("insert_arrivals=True but len(aqms_arrival)==0!!")

        if assocaro_adds:
            session.bulk_save_objects(assocaro_adds)

    if insert_amplitudes:
        aqms_amplitudes = []
        assocamm_adds = []
        assocamo_adds = []
        # MTH: ATODO: Need to test if these amplitudes are in fact coda measurements and fill coda table accordingly
        for amplitude in event.amplitudes:
            ampid = ampids.pop(0)
            auth_amp = auth
            #if getattr(amplitude.pick_id.get_referred_object(), 'creation_info', None) and \
            #getattr(amplitude.pick_id.get_referred_object().creation_info, 'agency_id', None):
            if getattr(amplitude, 'creation_info', None) and \
            getattr(amplitude.creation_info, 'agency_id', None):
                auth_amp = amplitude.creation_info.agency_id.upper()
            else:
                logger.debug("Amplitude doesn't have creation_info --> use origin auth=[%s] for auth_amp" % (auth))

            logger.debug("Convert amplitude: ampid=[%s] auth=[%s]" % (ampid, auth))
            aqms_amplitudes.append(obspy_amplitude_to_aqms(amplitude, ampid=ampid, auth=auth, subsource=subsource))

            st_mag = None
            #for station_mag in event.station_magnitudes:
            for station_mag in event.station_magnitudes:
                if amplitude.resource_id == station_mag.amplitude_id:
                    st_mag = station_mag
                    break
            # assocamm - associates amps with mag
            assocamm_adds.append( obspy_amplitude_to_assocamm(ampid=ampid, magid=magid,  auth=auth, station_mag=st_mag, subsource=subsource) )
            # assocamo - associates amps with origin
            # Note: This will not set fields: delta & seaz in assocamo
            #       since they are not present in obspy amplitudes
            #       The fix would be to relate ampid scnl to arrival scnl and
            #       pull delta/seaz from the corresponding arrival
            assocamo_adds.append( obspy_amplitude_to_assocamo(ampid=ampid, orid=orid,  auth=auth, subsource=subsource) )
        if aqms_amplitudes:
            session.bulk_save_objects(aqms_amplitudes)
            session.bulk_save_objects(assocamm_adds)
            session.bulk_save_objects(assocamo_adds)
            logger.info("Insert %d amps ampid:%d -to- %d" % (len(aqms_amplitudes), aqms_amplitudes[0].ampid, aqms_amplitudes[-1].ampid))

# 5. Commit the session
    logger.info("Commit the session insert_arrivals=%s" % insert_arrivals)
    try:
        logger.info("commit the session")
        session.commit()
    except exc.SQLAlchemyError as e:
        logger.error("Could not commit session:%s" % e)
        logger.error("Rollback session and exit")
        session.rollback()
        raise
    finally:
        session.close()


    # Use stored proc to set preferred magnitude
    if insert_magnitude:
        # MTH: Use stored proc to determine if this mag is preferred
        #      If so, the procedure will update eventprefmag, event.prefmagid and origin.prefmagid
        logger.info("Set the preferred magnitude to  magid=%d" % magid)
        try:
            engine = engine_from_config(config)
            connection = engine.raw_connection()
            cursor = connection.cursor()
            retval = None
            retval = cursor.callfunc("epref.setprefmag_magtype", int, [evid,magid])
            logger.debug("Stored proc epref.setprefmag_magtype")
            if retval == 2:
                logger.info("Stored procedure setprefmag returned val=%d" % retval)
            elif retval == 1:
                logger.warning("Stored procedure setprefmag returned val=%d" % retval)
            else:
                logger.error("Stored procedure setprefmag returned val=%d" % retval)
            cursor.close()
            connection.commit()
        finally:
            connection.close()

    logger.info("==== End of pdl_to_aqms  ============================================")

    return


from sqlalchemy import Sequence
from sqlalchemy import create_engine, engine_from_config
from sqlalchemy.orm import sessionmaker

Session = None

def createSession(config):
    global Session

    if Session:
        logger.warning("Session is already created")
        return

    try:
        #engine = engine_from_config(config, echo=True)
        engine = engine_from_config(config)
        Session = sessionmaker(bind=engine)
    except :
        raise

    #return Session

def getSequence(name, count):
    sequences = {'event':'evseq',
                 'origin':'orseq',
                 'arrival':'arseq',
                 'amplitude':'ampseq',
                 'magnitude':'magseq',
                 'mechanism':'mecseq',
                 'unassocamp':'unassocseq',
                 'remark':'commseq',
                 }

    session = Session()

    nextids = []

    if name in sequences:
        seq = Sequence(sequences[name])
        for i in range(count):
            try:
                nextids.append(session.execute(seq))
            except exc.SQLAlchemyError as e:
                logger.error("Caught SQLalchemyError:%s" % e)
                logger.error("*** Insert event FAILED --> Early Exit!!")
                exit(2)
    else:
        logger.error("getSequence: Error - Unrecognized sequence name=[%s]" % name)

    session.close()
    return nextids

def query_assocevents(evid, evidassoc, session=None):
    close_session = False
    if session is None:
        session = Session()
        close_session = True

    found = session.query(Assocevents).get((evid, evidassoc))

    if close_session:
        session.close()

    if found:
        logger.info("query_assocevents: found.evid=%s found.evidassoc=%s" % (found.evid, found.evidassoc))
        return True
    else:
        return False

def query_evid(evid, session=None):
    '''
        Look up event evid in the origin table
        If found, return the preferred origin
    '''
    close_session = False
    if session is None:
        session = Session()
        close_session = True

    rows = session.query(Origin).join(Eventprefor).filter(Origin.evid==evid).all()

    if close_session:
        session.close()

    logger.info("query_evid: query for evid=%s returned %d results" % (evid, len(rows)))

    if rows:
        return rows[0]

    return None


def query_for_pdl_event_in_aqms(pdl_id):
    '''
        Query origin table for origin.locevid == pdl_id
        If found return latest origin with this locevid
    '''
    session = Session()
    # found = session.query(Origin).filter(Origin.locevid==pdl_id).all()
    # Get the most recently loaded origin that has this locevid (it may not be preferred):
    found = session.query(Origin).filter(Origin.locevid==pdl_id).order_by(Origin.lddate.desc()).all()
    session.close()

    orig = found[0] if found else None
    return orig

def fix_unassoc_amp_leap_seconds(amp_dict):
    '''
        If db epochtimebase.base='T' then add leapsecs to amp datetime + wstart fields
    '''
    epoch = amp_dict['wstart'].timestamp
    orig_utc = amp_dict['wstart']
    session = Session()
    # Query to see if this db is applying leapsecs or not
    # Note that epochtimebase table typically has:
    # ondate='1900-01-01' (timestamp=-2208970800) and offdate='3000-01-01' (timetamp=32503698000)
    rows = session.query(Epochtimebase).all()
    base = None
    for row in rows:
        on  = row.ondate.timestamp()
        off = row.offdate.timestamp()
        if epoch >= on and epoch <= off:
            base = row.base
            break

    if base == 'T':
        logger.debug("Epochtimebase base=T ==> Apply leapsecs")
        epoch_base=list(session.execute("select truetime.nominal2true(%d) from dual" % int(epoch)))
        fixed_epoch = epoch_base[0][0]
        leap_secs = fixed_epoch - int(epoch)
        logger.info("fix_unassoc_amp wstart=[%s] --> epoch=[%s] + leap_secs=[%d] = [%s]" % \
                    (amp_dict['wstart'], epoch, leap_secs, (amp_dict['wstart'].timestamp + leap_secs)))
        amp_dict['wstart'] = amp_dict['wstart'].timestamp + leap_secs
        amp_dict['datetime'] = amp_dict['datetime'].timestamp + leap_secs

    elif base == 'N':
        logger.debug("Epochtimebase base=N ==> Do NOT apply leapsecs")
    else:
        logger.error("Epochtimebase base=%s ==> Cannot determine whether to apply leapsecs. Exitting!" % base)
        session.close()
        exit(2)

    session.close()
    return


def fix_leap_seconds(origin, event):
    '''
        If db epochtimebase.base='T' then add leapsecs to origin + pick times
    '''
    epoch = origin.time.timestamp
    session = Session()
    # Query to see if this db is applying leapsecs or not
    # Note that epochtimebase table typically has:
    # ondate='1900-01-01' (timestamp=-2208970800) and offdate='3000-01-01' (timetamp=32503698000)
    rows = session.query(Epochtimebase).all()
    base = None
    for row in rows:
        on  = row.ondate.timestamp()
        off = row.offdate.timestamp()
        if epoch >= on and epoch <= off:
            base = row.base
            break

    if base == 'T':
        logger.debug("Epochtimebase base=T ==> Apply leapsecs")
        epoch_base=list(session.execute("select truetime.nominal2true(%d) from dual" % int(epoch)))
        fixed_epoch = epoch_base[0][0]
        leap_secs = fixed_epoch - int(epoch)
        otime = origin.time
        origin.time += leap_secs
        logger.info("fix_leap: origin.time=[%s] + leap_secs=[%d] = new origin.time=[%s]" % \
                    (otime, leap_secs, origin.time))
        for pick in event.picks:
            pick.time += leap_secs
    elif base == 'N':
        logger.debug("Epochtimebase base=N ==> Do NOT apply leapsecs")
    else:
        logger.error("Epochtimebase base=%s ==> Cannot determine whether to apply leapsecs. Exitting!" % base)
        session.close()
        exit(2)

    session.close()
    return

def read_quakemlfile(quakemlfile):

    event = None
    origin = None
    magnitude = None
    focal_mechanism = None

    try:
        #cat = read_events(quakemlfile, format="quakeml")
        # Read quakeml into string and replace anssevent tags:
        with open(quakemlfile, 'r') as file:
            xml = file.read()
        anssInternal = False
        if 'anssevent:internalEvent' in xml:
            anssInternal = True
            logger.info("This is an anssevent:internalEvent quakeml")
            xml = xml.replace('anssevent:internalEvent', 'event')
            #xml = file.read().replace('anssevent:internalEvent', 'event')
        cat = read_events(io.BytesIO(bytes(xml, encoding='utf-8')), format="quakeml")
        event = cat[0]
    except:
        logger.error("Problem reading quakemlfile=[%s]" % quakemlfile)
        raise

    magnitude = event.preferred_magnitude()
    origin = event.preferred_origin()
    if event.focal_mechanisms:
        focal_mechanism = event.preferred_focal_mechanism() or event.focal_mechanisms[0]

    if event.preferred_origin() is None and event.origins is not None:
        mww_origin = None
        mww_trigger_origin = None
        for origin in event.origins:
            foo = origin.resource_id.id.split("/")[-1]
            if foo == 'mww':
                mww_origin = origin
            elif foo == 'mww_trigger':
                mww_trigger_origin = origin
            else:
                logger.warning("Unknown origin id:%s" % foo)
        if mww_trigger_origin:
            logger.info("Set origin = mww_trigger_origin")
            origin = mww_trigger_origin
        elif mww_origin:
            logger.info("Set origin = mww_origin")
            origin = mww_origin

    return event, origin, magnitude, focal_mechanism

def compare_origins(quakeml_origin=None, aqms_origin=None):
    '''
        Compare incoming quakeml_origin to db aqms_origin for same PDL event
    '''

    origin = quakeml_origin
    found_orig = aqms_origin
    #logger.info("quakeml_origin.latitude=%f aqms_origin.lat=%f\n" % 
                #(quakeml_origin.latitude, float(aqms_origin.lat)))
    # Determine if origin is the same as the db origin (=found_orig)
    #timestamp in seconds, depth in m

    x = [origin.time.timestamp, origin.latitude, origin.longitude, origin.depth]
    y = [float(found_orig.datetime), float(found_orig.lat), float(found_orig.lon), float(found_orig.depth) * 1e3]
    close = np.isclose(x, y, rtol=1e-09, atol=0.0)
    if np.all(close):
        logger.info("%s: found_orig:%s is perfect match, do nothing" % (fname, found_orig.orid))
        return True

    return False


def associate_local_origin(PDL_orig, RSN_source, assoc_thresh):
    '''
    Get a list of RSN origins that match within thresh
        of this PDL origin
    '''

    fname = 'associate_local_origin'

    local_origins = query_origins_around_origin_time(PDL_orig, assoc_thresh)
    for orig in local_origins:
        logger.debug("check orig orid:%d auth:%s subsource:%s" % (orig.orid, orig.auth, orig.subsource))
    local_origins = [origin for origin in local_origins if origin.subsource != 'PDL' and origin.auth == RSN_source]

    assoc_origins = []

    if local_origins:
        for orig in local_origins:
            logger.info("PDL evid:%d orid:%d  ==> Compare to candidate RSN evid:%d orid:%d" %
                          (PDL_orig.evid, PDL_orig.orid, orig.evid, orig.orid))
            if compare_aqms_origins(PDL_orig, orig, assoc_thresh):
                logger.info("PDL evid:%d orid:%d OT:%s <%.2f, %.2f> h=%.1f [auth:%s] (locevid:%s)==> Assocs to:" %
                            (PDL_orig.evid, PDL_orig.orid, UTCDateTime(PDL_orig.datetime),
                             PDL_orig.lat, PDL_orig.lon, PDL_orig.depth, PDL_orig.auth, PDL_orig.locevid))
                logger.info("RSN evid:%d orid:%d OT:%s <%.2f, %.2f> h=%.1f [auth:%s] (locevid:%s)" %
                            (orig.evid, orig.orid, UTCDateTime(orig.datetime),
                             orig.lat, orig.lon, orig.depth, orig.auth, orig.locevid))
                assoc_origins.append(orig)
    else:
        logger.info("%s: PDL evid=%d orid=%d did not associate with any RSN origins in db" %
                    (fname, PDL_orig.evid, PDL_orig.orid))

    return assoc_origins


class thresh:
    def __init__(self, time, km, depth):
        self.time = time
        self.km = km
        self.depth = depth

class diff:
    def __init__(self, dt, dx, dz):
        self.dt = dt
        self.dx = dx
        self.dz = dz

def query_origins_around_origin_time(target_origin, thresh):
    '''
        return list of origins in local db within thresh
            of target_origin
    '''
    session = Session()
    t1 = float(target_origin.datetime) - thresh.time
    t2 = float(target_origin.datetime) + thresh.time

    logger.debug("query_origins around origin.datetime:%s +/- %s" % 
                 (target_origin.datetime, thresh.time))
    #rows = session.query(Origin).join(Eventprefor).filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
    # MTH: PRSN eventprefor is not set for RT1, Jiggle, EBird events!
    #rows = session.query(Origin).filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
    #rows = session.query(Origin, Event).filter(Origin.evid == Event.evid).filter(Event.selectflag == 1).\
                         #filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()
    rows = session.query(Origin).join(Event, Origin.evid == Event.evid).filter(Event.selectflag == 1).\
                         filter(Origin.datetime>=t1).filter(Origin.datetime<=t2).all()


    logger.debug("query_origins returned %d rows" % len(rows))
    #             filter(Origin.subsource != 'PDL').filter(Origin.auth == 'RSN_source').all()
    session.close()
    return rows

def calc_origin_diff(orig1, orig2):

    tol = 1e-6

    lat1 = float(orig1.lat)
    lon1 = float(orig1.lon)
    lat2 = float(orig2.lat)
    lon2 = float(orig2.lon)

    t1 = float(orig1.datetime)
    t2 = float(orig2.datetime)

    h1 = float(orig1.depth)
    h2 = float(orig2.depth)

    if (np.fabs(lat1 - lat2) < tol) \
       and (np.fabs(lon1 - lon2) < tol):
        dist = 0.
    else:
        dist, az, baz = gps2dist_azimuth(lat1, lon1, lat2, lon2)

    dt = np.fabs(t1 - t2)
    dz = np.fabs(h1 - h2)/1e3
    dx = dist/1e3

    return diff(dt, dx, dz)

def compare_aqms_origins(orig1, orig2, thresh):
    '''
        return True if orig1 and orig2 are within thresh.{time, km, depth}
    '''

    od = calc_origin_diff(orig1, orig2)

    if (od.dt < thresh.time) \
       and (od.dz < thresh.depth) \
       and (od.dx < thresh.km):
        return True
    else:
        return False

import re
def parse_eventid(eventid):
    '''
    Example inputs:
         input              output
        eventid         prefix      eid
      =====================================
      us400063yy        us        400063yy
      usauto400063yy    usauto    400063yy
      us_400063yy_mww   us        400063yy
      us_a000es01_mww   us        a000es01
      nc73554385_fm1    nc        73554385
    '''

    p1 = re.compile(r'[a-z]+_+')
    p2 = re.compile(r'[a-z]+')

    match = p1.match(eventid)
    if match is None:
        match = p2.match(eventid)

    if match:
        prefix = match.group()
        prefix = prefix.replace('_','')
        eid = eventid[match.end():]
        indx = eid.find('_')
        if indx > 0:
            eid = eid[:indx]
    else:
        logger.error("parse_eventid eventid=%s is unexpected format that is NOT handled!" % eventid)
        exit(2)

    return prefix, eid


def check_eventids(args, RSN_source, keep_them_separated=False):
    '''
        MTH: I see several cases coming through PDL:
            1. --source=us --code=us6000dxnd --eventids = [us6000dxnd]
                >pdl_id=6000dxnd + auth=us
            2. --source=nn --code=nn00803678 --preferred-eventid=nn00803678 --eventids = [nn00803678, us6000dxnd]
                >separate into pdl_id=00803678 + auth=nn + other_ids=[6000dxnd]
            3. --source=us --code=us6000dxna --preferred-eventid=nc73541781 --eventids = [nc73541781, us6000dxna]
                >separate into pdl_id=6000dxna + auth=us + RSN_pdl_id=73541781
            4. --source=us --code=us6000dykc --preferred-eventid=nn00804169 --eventids = [nn00804169, us6000dykc]
                >separate into pdl_id=6000dykc + auth=us + other_ids=[00804169]

    Sort the eventids + codes into:
    pdl_id, RSN_pdl_id, other_pdlids

    other_pdlids - from eventids with args.code removed
    '''

    #prefix, pdl_id = parse_eventid(args.code) #prefix should be same as args.source for this
    pdl_prefix, pdl_id = parse_eventid(args.code) #prefix should be same as args.source for this

    RSN_pdl_id = None
    other_pdlids = []
    eventids = args.eventids.copy()
    if args.code in eventids:
        eventids.remove(args.code)
    else:
        logger.warning("eventids list does NOT CONTAIN args.code=%s" % args.code)
    for i, eventid in enumerate(eventids):
        prefix, eid = parse_eventid(eventid)

        if prefix.upper() == RSN_source.upper():
            RSN_pdl_id = eid
            logger.info("check_eventids: eventids[%d]:%s ==> prefix:%s matches RSN_source:%s RSN_pdl_id=%s" %
                        (i, eventid, prefix, RSN_source, RSN_pdl_id))
        else:
            #if eventid != args.preferred_eventid:
            # If keep_them_separated ==> Only add eids with same first 2-char code to other_pdlids
            #    so that "us" and "usauto" remain on same evid
            #        and "ci" and "cidev" remain on same evid
            #        but "nn" and "us" separate (for same event)
            if keep_them_separated:
                if prefix[0:2] != pdl_prefix[0:2]:
                    continue

            other_pdlids.append(eid)
            logger.info("check_eventids: Add to other_pdlids: eventids[%d]:%s ==> prefix:%s pdl_id:%s" %
                        (i, eventid, prefix, eid))

    return pdl_id, RSN_pdl_id, other_pdlids


def check_for_RSN_evid(args, RSN_source=None):
    '''
        See if args.eventids has an id that begins with the
          2-char RSN code and if so, pull the rest of the evid
          to see if it corresponds to evids in the local (RSN) db.
    '''
    if len(args.eventids) > 1:
        #logger.info("%s: Attention pdl_type:%s code:%s preferred_eventid:%s has >1 eventids:%s" %
                    #(fname, args.type, args.code, args.preferred_eventid, args.eventids))
        found_eventid = None
        for evid in args.eventids:
            if evid[0:2] == RSN_source:
                found_eventid = evid
                break
        if found_eventid:
            preferred = False
            if found_eventid == args.preferred_eventid:
                preferred = True
            RSN_evid = found_eventid[2:]
            logger.info("%s: RSN_source=[%s] RSN_evid=[%s] preferred:[%s]" %
                        (fname, RSN_source, RSN_evid, preferred))
            return RSN_evid

    return None

def handle_unassociated_amp(quakemlfile, use_db=0):

    xml_amps_dir = quakemlfile
    amps = scan_xml_amps(xml_amps_dir)
    logger.info("%s: pdl_type:%s scan for amps in xml_dir=[%s] returned:[%d] unassoc amps" % \
                (fname, pdl_type, xml_amps_dir, len(amps)))
    if use_db:
        session = Session()
        ampids = getSequence('unassocamp', len(amps))
        AQMS_amps = []
        for amp in amps:
            ampid = ampids.pop(0)
            fix_unassoc_amp_leap_seconds(amp)
            AQMS_amps.append(unassoc_amplitude_to_aqms(amp, ampid=ampid, auth=auth, subsource=subsource))
        session.bulk_save_objects(AQMS_amps)
        session.commit()
        session.close()
        logger.info("%s: Done inserting unassocamps --> Exit" % fname)

    return

def set_gtype(config, source, lat, lon):

    '''
    returns:
        l - if lat,lon within authoritative region for this source (RSN)
        r - if lat,lon not within authoritative region for this source (RSN)
        None - if it can't be determined (e.g., if source not set)
    '''

    gtype = None

    logger.info("set_gtype called with source=%s lat=%f lon=%f" % (source, lat, lon))

    try:
        engine = engine_from_config(config)
        connection = engine.raw_connection()
        cursor = connection.cursor()
        query = "select geo_region.inside_border('%s', %f, %f) from dual" % \
                (source, lat, lon);
        cursor.execute(query)
        retval = cursor.fetchone()[0]
        logger.info("query:[%s] returned val=%d" % (query, retval))
        if retval == 1:
            gtype = 'l'
        elif retval == 0:
            gtype = 'r'
        else:
            logger.warning("gtype was not successfully set by SP!")
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    logger.info("set_gtype return gtype=%s" % gtype)
    return gtype


if __name__ == "__main__":
    main()

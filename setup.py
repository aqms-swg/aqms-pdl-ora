"""A setuptools based setup module for aqms-pdl"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from codecs import open
from os import path
from setuptools import setup, find_packages

import aqms_pdl
#import versioneer

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as readme_file:
    readme = readme_file.read()

with open(path.join(here, 'HISTORY.rst'), encoding='utf-8') as history_file:
    history = history_file.read().replace('.. :changelog:', '')

requirements = [
    # TODO: put package requirements here
    #'click',
    'numpy',
    'obspy',
    'SQLAlchemy',
    'cx_Oracle',
    'PyYAML',
]

test_requirements = [
    # TODO: put package test requirements here
        'tox',
        'pytest',
        'pytest-runner',
]

cli_requirements = [
        #'click',
]
develop_requirements = test_requirements + cli_requirements


setup(
    name='aqms-pdl',
    #version=versioneer.get_version(),
    version=aqms_pdl.__version__,
    #cmdclass=versioneer.get_cmdclass(),
    description="Listens for PDL messages and inserts quakeml into AQMS db",
    long_description=readme + '\n\n' + history,
    author="Mike Hagerty",
    author_email='mhagerty@isti.com',
    url='https://gitlab.isti.com/mhagerty/aqms-pdl',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    entry_points={
        'console_scripts':[
            'pdl2aqms=aqms_pdl.pdl_to_aqms:main',
            'pdl-to-aqms=aqms_pdl.pdl_to_aqms:main',
            'pdl-to-SA=aqms_pdl.pdl_to_SA:main',
            'qml2aqms=aqms_pdl.qml_to_aqms:main',
            'play-aqms=aqms_pdl.play_aqms:main',
            ],
        },
    include_package_data=True,
    package_data={
        #'seismic_format':['formats/format*']
    },
    #data_files=[('my_data', ['test_data/*'])],
    install_requires=requirements,
    license="MIT",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    extras_require={ # MTH: eg, >pip install pkg[cli]
        'cli': cli_requirements,
        'develop': develop_requirements,
        }
)
